var App = App || {};
App.Bilderraten = (function () {
    "use strict";
    /* eslint-env browser, fabric, jquery */
    var timer,
        interval,
        playing,
        seconds,
        message,
        canvas,
        view,
        score,
        points,
        text,
        container,
        mapName,
        arrayName,
        promi,
        imagesArray = [],
        WIDTH = 500,
        HEIGHT = 380,
        HEARTS = 3,
        countHeartsDown,
        radius,
        stopp,
        inputField,
        resultField,
        result,
        correctAnswer,
        next,
        value,
        buttonClicked,
        randomNumber,
        imagesSolved,
        totalImages,
        CORRECT = "RICHTIG",
        WRONG = "FALSCH",
        LOST = "VERLOREN",
        WON = "GEWONNEN",
        NEXT = "Tippe zum Fortfahren",
        PROMI_TEXT = "Der gesuchte Promi ist: ",
        CITY_TEXT = "Die gesuchte Stadt ist: ",
        STOPP = "Stopp",
        heartImage = "url(res/drawables/heart.png)",
        snoopyImage = "url('res/drawables/snoopy.png')";

    function setupContainer() {
        container = document.querySelector("#bilderraten-container-canvas");
        container.style.display = "block";
    }

    function setupCanvas() {
        canvas = new fabric.Canvas("bilderraten-canvas");
        canvas.setWidth(WIDTH);
        canvas.setHeight(HEIGHT);
        canvas.setBackgroundImage("", canvas.renderAll.bind(canvas), {
            width: canvas.width,
            height: canvas.height
        });
    }

    function checkIfGameOverOrNot() {
        if(countHeartsDown === HEARTS) {
            value = LOST;
        } else {
            value = WRONG;
        }
    }

    function reduceHearts() {
        view.childNodes[HEARTS - countHeartsDown].classList.add("invisible");
        countHeartsDown++;
        checkIfGameOverOrNot();
    }

    function pointsWon() {
        if (seconds >= 10) {
            points += 5;
        } else if (seconds >= 7) {
            points += 3;
        } else if (seconds >= 1) {
            points += 1;
        }
    }

    function pointsLost() {
        points -= 5;
    }

    function checkIfGameWonOrNot() {
        if (imagesSolved === totalImages) {
            value = WON;
        } else {
            value = CORRECT;
        }
    }

    function getRandomImage() {
        return fabric.util.getRandomInt(0, 51);
    }

    function checkIfImageAlreadyExists() {
        if($.inArray(randomNumber, imagesArray) !== -1) {
            while ($.inArray(randomNumber, imagesArray) !== -1) {
                randomNumber = fabric.util.getRandomInt(0, 51);
            }
        }
    }

    function putFilterOrNot(img) {
        if (arrayName === App.Arrays.getArrayPromis()) {
            img.filters.push(new fabric.Image.filters.Pixelate({
                blocksize: 7
            }));
            img.applyFilters(canvas.renderAll.bind(canvas));
        }
    }

    function setupFabricImage() {
        fabric.Image.fromURL(mapName[randomNumber], function(img) {
            img.scale(0.8).set({
                left: 30,
                top: 82,
                angle: -11,
                selectable: false,
                clipTo: function (ctx) {
                    ctx.arc(0, 0, radius, 0, Math.PI * 2, true);
                }
            });
            putFilterOrNot(img);
            canvas.add(img);
        });
    }

    function setupImage() {
        randomNumber = getRandomImage();
        checkIfImageAlreadyExists();
        imagesArray.push(randomNumber);
        promi = arrayName[randomNumber].toUpperCase();
        setupFabricImage();
    }

    function animate() {
        fabric.util.animate({
            startValue: Math.round(radius) === 10 ? 350 : 10,
            endValue: Math.round(radius) === 10 ? 10 : 350,
            duration: 15000,
            onChange: function(val) {
                radius = val;
                canvas.renderAll();
            },
            abort: function() {
                return buttonClicked;
            }
        });
    }

    function checkTest(playerResult) {
        var test = App.SpielfeldController.getTest();
        if (test === true) {
            var gameContainer = document.querySelector("#game-container");
            gameContainer.style.display = "none";
            App.Description.init();
        } else {
            App.SpielfeldController.gameFinished(playerResult);
        }
    }

    function onResultFieldClicked() {
        message.finish();
        if (result.innerHTML === LOST) {
            clearInterval(interval);
            checkTest(false);
        } else if (result.innerHTML === WON) {
            clearInterval(interval);
            checkTest(true);
        } else {
            buttonClicked = false;
            container.style.display = "block";
            stopp.style.display = "block";
            resultField.style.display = "none";
            result.innerHTML = "";
            correctAnswer.innerHTML = "";
            next.innerHTML = "";
            canvas.clear().renderAll();
            setupImage();
            animate();
            seconds = 14;
            playing = true;
        }
    }

    function setupNextText() {
        next = document.createElement("span");
        next.classList.add("continue");
        next.innerHTML = NEXT;
        resultField.appendChild(next);
    }

    function setupCorrectAnswer() {
        correctAnswer = document.createElement("span");
        correctAnswer.classList.add("correct");
        correctAnswer.innerHTML = text + promi;
        resultField.appendChild(correctAnswer);
    }

    function setupResult() {
        result = document.createElement("span");
        result.classList.add("feedback");
        result.innerHTML = value;
        resultField.appendChild(result);
    }

    function showResultField() {
        message.html("<b>" + points + "</b><br>Punkte").show().fadeOut(4000);
        playing = false;
        resultField.style.display = "block";
        setupResult();
        setupCorrectAnswer();
        setupNextText();
        resultField.addEventListener("click", onResultFieldClicked, false);
    }

    function timeIsOver() {
        reduceHearts();
        container.style.display = "none";
        stopp.style.display = "none";
        timer.innerHTML = 0;
        pointsLost();
        showResultField();
    }

    function setTime() {
        if (!playing) {
            return;
        }
        if (seconds > 0) {
            timer.innerHTML = seconds;
            seconds--;
        } else {
            timeIsOver();
        }
    }

    function checkKey(event) {
        var enter = event.keyCode;
        if (enter === 13) {
            var playerResult = inputField.value.toLowerCase();
            if (playerResult === arrayName[randomNumber]) {
                imagesSolved++;
                pointsWon();
                score.innerHTML = imagesSolved + "/" + totalImages;
                checkIfGameWonOrNot();
                showResultField();
            } else {
                pointsLost();
                reduceHearts();
                showResultField();
            }
            inputField.value = "";
            inputField.style.display = "none";
            container.style.display = "none";
            document.onkeydown = null;
        }
    }

    function onStoppButtonClicked() {
        buttonClicked = true;
        playing = false;
        stopp.style.display = "none";
        inputField = document.querySelector("#answer-bilderraten");
        inputField.style.display = "block";
        inputField.focus();
        document.onkeydown = checkKey;
    }

    function setupButton(){
        stopp = document.querySelector("#button-stopp");
        stopp.innerHTML = STOPP;
        stopp.addEventListener("click", onStoppButtonClicked, false);
    }

    function setupHearts() {
        for (var i = 0; i < HEARTS; i++) {
            var heart = document.createElement("span");
            heart.classList.add("heart");
            heart.style.backgroundImage = heartImage;
            view.appendChild(heart);
        }
    }

    function setupScore() {
        score = document.createElement("span");
        score.classList.add("score");
        score.innerHTML = imagesSolved + "/" + totalImages;
        view.appendChild(score);
    }

    function setupView() {
        view = document.querySelector("#view");
    }

    function getNumberOfImages() {
        totalImages = fabric.util.getRandomInt(6, 8);
    }

    function getMap() {
        var randomMapNumber = fabric.util.getRandomInt(0, 1);
        if (randomMapNumber === 0) {
            text = PROMI_TEXT;
            arrayName = App.Arrays.getArrayPromis();
            return App.Arrays.getMapPromis();
        } else {
            text = CITY_TEXT;
            arrayName = App.Arrays.getArrayCities();
            return App.Arrays.getMapCities();
        }
    }

    function setupSnoopy() {
        var snoopy = document.querySelector("#snoopy-flight");
        snoopy.style.backgroundImage = snoopyImage;
    }

    function setupTime() {
        timer = document.querySelector("#time");
        timer.innerHTML = seconds;
        setTime();
        interval = setInterval(setTime, 1000);
    }

    function setupResultField() {
        resultField = document.querySelector("#result-bilderraten");
    }

    function setupMessage() {
        message = $("#game-message");
        message.css({
            top: 370,
            right: 370
        });
    }

    function getPoints() {
        return points;
    }

    function init() {
        points = 0;
        seconds = 14;
        imagesSolved = 0;
        countHeartsDown = 0;
        buttonClicked = false;
        playing = true;
        document.body.style.backgroundImage = "url('res/drawables/101504.jpg')";
        setupMessage();
        setupResultField();
        setupTime();
        mapName = getMap();
        setupContainer();
        setupCanvas();
        setupImage();
        getNumberOfImages();
        setupSnoopy();
        setupView();
        setupScore();
        setupHearts();
        setupButton();
        animate();
    }

    return {
        init: init,
        getPoints: getPoints
    };
}());
