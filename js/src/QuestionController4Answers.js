var App = App || {};
App.QuestionController4Answers = function (feedback) {
    "use strict";
    /* eslint-env browser, fabric, jquery */
    var question,
        questionField,
        answer,
        answerField,
        result,
        correctAnswer,
        score,
        jsonObject,
        randomQuestions,
        randomHearts,
        totalNum,
        view,
        MAX = 4,
        text,
        answerArray = [],
        countHeartsDown,
        questionsSolved,
        callback = feedback,
        RIGHT = "richtig",
        FALSE = "falsch",
        LOST = "verloren",
        LOST_FEEDBACK = "Das Spiel ist verloren. Alle Leben sind verbraucht.",
        GREEN = "rgb(0, 255, 0)",
        RED = "rgb(255, 0, 0)",
        BLUE = "rgb(30, 150, 255)",
        blue = "rgba(30, 150, 255, 0.7)",
        URL = "js/json/JSON_Object_4_Answers.json",
        heartImage = "url(res/drawables/heart.png)",
        snoopyImage = "url('res/drawables/snoopy2.png')";

    function getCorrectAnswer() {
        for (var i = 0; i < MAX; i++) {
            if (answerField.childNodes[i].innerHTML === jsonObject[randomQuestions].correct) {
                correctAnswer = answerField.childNodes[i];
            }
        }
        return correctAnswer;
    }

    function onAnswerOut(event) {
        event.target.style.backgroundColor = BLUE;
    }

    function onAnswerOver(event) {
        event.target.style.backgroundColor = blue;
    }

    function removeMouseEventListener() {
        for (var i = 0; i < MAX; i++) {
            answerField.childNodes[i].removeEventListener("mouseover", onAnswerOver, false);
            answerField.childNodes[i].removeEventListener("mouseout", onAnswerOut, false);
        }
    }

    function setColors(event) {
        event.target.style.backgroundColor = RED;
        correctAnswer = getCorrectAnswer();
        correctAnswer.style.backgroundColor = GREEN;
        removeMouseEventListener();
    }

    function reduceHearts() {
        view.childNodes[randomHearts - countHeartsDown].remove();
        countHeartsDown++;
    }

    function removeEventListener() {
        for (var i = 0; i < MAX; i++) {
            answerField.childNodes[i].removeEventListener("click", onAnswerClicked, false);
        }
    }

    function wrong(event) {
        reduceHearts();
        setColors(event);
        if(countHeartsDown === randomHearts) {
            result = LOST;
            text = LOST_FEEDBACK;
        } else {
            result = FALSE;
            text = question.innerHTML;
        }
    }

    function right() {
        questionsSolved++;
        result = RIGHT;
        text = question.innerHTML;
        event.target.style.backgroundColor = GREEN;
        removeMouseEventListener();
    }

    function onAnswerClicked(event) {
        questionField.classList.add("hidden");
        removeEventListener();
        if (event.target.innerHTML === jsonObject[randomQuestions].correct) {
            right();
        } else {
            wrong(event);
        }
        callback(result, score, questionsSolved, totalNum, text);
    }

    function setAnswers () {
        for (var i = 0; i < MAX; i++) {
            answerField.childNodes[i].innerHTML = jsonObject[randomQuestions].options[i];
            answerField.childNodes[i].style.backgroundColor = BLUE;
        }
    }

    function addEventListener() {
        for (var i = 0; i < MAX; i++) {
            answerField.childNodes[i].addEventListener("click", onAnswerClicked, false);
            answerField.childNodes[i].addEventListener("mouseover", onAnswerOver, false);
            answerField.childNodes[i].addEventListener("mouseout", onAnswerOut, false);
        }
    }

    function checkIfQuestionAlreadyExists() {
        if($.inArray(randomQuestions, answerArray) !== -1) {
            while ($.inArray(randomQuestions, answerArray) !== -1) {
                randomQuestions = fabric.util.getRandomInt(0, 112);
            }
        }
    }

    function setQuestion() {
        randomQuestions = fabric.util.getRandomInt(0, 112);
        checkIfQuestionAlreadyExists();
        answerArray.push(randomQuestions);
        question.innerHTML = jsonObject[randomQuestions].question;
        questionField.classList.remove("hidden");
    }

    function createAnswers() {
        for (var i = 1; i <= MAX; i++) {
            answer = document.createElement("span");
            answer.classList.add("answer" + i);
            answer.innerHTML = jsonObject[randomQuestions].options[i - 1];
            answer.addEventListener("click", onAnswerClicked, false);
            answer.addEventListener("mouseover", onAnswerOver, false);
            answer.addEventListener("mouseout", onAnswerOut, false);
            answerField.appendChild(answer);
        }
    }

    function createQuestion() {
        question = document.createElement("span");
        question.classList.add("question");
        setQuestion();
        questionField.appendChild(question);
    }

    function processServerResponse(data) {
        jsonObject = data;
        createQuestion();
        createAnswers();
    }

    function getJSON() {
        $.ajax({
            type: "GET",
            url: URL,
            dataType: "json",
            success: processServerResponse
        });
    }

    function setupAnswerField() {
        answerField = document.querySelector("#four-answers-answerField");
    }

    function setupQuestionField() {
        questionField = document.querySelector("#questionField");
    }

    function setupHearts() {
        for (var i = 0; i < randomHearts; i++) {
            var heart = document.createElement("span");
            heart.classList.add("heart");
            heart.style.backgroundImage = heartImage;
            view.appendChild(heart);
        }
    }

    function setupScore() {
        score = document.createElement("span");
        score.classList.add("score");
        score.innerHTML = questionsSolved + "/" + totalNum;
        view.appendChild(score);
    }

    function setupView() {
        view = document.querySelector("#view");
    }

    function setupSnoopy() {
        var snoopy = document.querySelector("#snoopy");
        snoopy.style.backgroundImage = snoopyImage;
    }

    function getNumberOfHearts() {
        randomHearts = fabric.util.getRandomInt(3, 5);
    }

    function getNumberOfQuestions() {
        totalNum = fabric.util.getRandomInt(7, 10);
    }

    function getQuestionField() {
        return questionField;
    }

    function getCountHeartsDown() {
        return countHeartsDown;
    }

    function getRandomHearts() {
        return randomHearts;
    }

    function init() {
        countHeartsDown = 0;
        questionsSolved = 0;
        getNumberOfQuestions();
        getNumberOfHearts();
        setupSnoopy();
        setupView();
        setupScore();
        setupHearts();
        setupQuestionField();
        setupAnswerField();
        getJSON();
    }

    return {
        init: init,
        getCountHeartsDown: getCountHeartsDown,
        getRandomHearts: getRandomHearts,
        setQuestion: setQuestion,
        setAnswers: setAnswers,
        addEventListener: addEventListener,
        getQuestionField: getQuestionField,
        reduceHearts: reduceHearts,
        removeEventListener: removeEventListener
    };
};
