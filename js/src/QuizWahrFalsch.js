var App = App || {};
App.QuizWahrFalsch = (function () {
    "use strict";
    /* eslint-env browser, jquery */
    var question,
        timer,
        seconds,
        points,
        message,
        questionField,
        resultField,
        answer,
        correct,
        next,
        done,
        interval,
        playing,
        RIGHT = "richtig",
        LOST = "verloren",
        WON = "gewonnen",
        ERROR = "fehler",
        NEXT = "Tippe zum Fortfahren",
        TIMEDOUT = "Die Zeit ist abgelaufen!",
        WON_FEEDBACK = "Das Level wurde erfolgreich abgeschlossen!";

    function gameGoesOn() {
        resultField.classList.add("hidden");
        answer.innerHTML = "";
        correct.innerHTML = "";
        next.innerHTML = "";
        seconds = 10;
        playing = true;
        question.setQuestion();
        question.addEventListener();
    }

    function checkTest(result) {
        var test = App.SpielfeldController.getTest();
        if (test === true) {
            var gameContainer = document.querySelector("#game-container");
            gameContainer.style.display = "none";
            App.Description.init();
        } else {
            App.SpielfeldController.gameFinished(result);
        }
    }

    function nextQuestion() {
        message.finish();
        if (done === LOST) {
            clearInterval(interval);
            checkTest(false);
        } else if (done === WON) {
            clearInterval(interval);
            checkTest(true);
        } else {
            gameGoesOn();
        }
    }

    function pointsWon() {
        if (seconds >= 7) {
            points += 5;
        } else if (seconds >= 4) {
            points += 3;
        } else if (seconds >= 1) {
            points += 1;
        }
    }

    function pointsLost() {
        points -= 5;
    }

    function showNextText() {
        next = document.createElement("span");
        next.classList.add("continue");
        next.innerHTML = NEXT;
    }

    function showCorrectAnswer(correctAnswer) {
        correct = document.createElement("span");
        correct.classList.add("correct");
        correct.innerHTML = correctAnswer;
    }

    function showFeedback(result) {
        answer = document.createElement("span");
        answer.classList.add("feedback");
        answer.innerHTML = result.toUpperCase();
    }

    function showResult(result, correctAnswer) {
        message.html("<b>" + points + "</b><br>Punkte").show().fadeOut(4000);
        playing = false;
        timer.innerHTML = 0;
        resultField.classList.remove("hidden");
        showFeedback(result);
        showCorrectAnswer(correctAnswer);
        showNextText();
        resultField.appendChild(answer);
        resultField.appendChild(correct);
        resultField.appendChild(next);
    }

    function checkIfGameIsLost() {
        var countHeartsDown = question.getCountHeartsDown();
        var randomHearts = question.getRandomHearts();
        if (countHeartsDown === randomHearts) {
            done = LOST;
        } else {
            done = ERROR;
        }
        showResult(done, TIMEDOUT);
    }

    function timeIsOver() {
        question.reduceHearts();
        questionField = question.getQuestionField();
        questionField.classList.add("hidden");
        pointsLost();
        question.removeEventListener();
        checkIfGameIsLost();
    }

    function setTime() {
        if (!playing) {
            return;
        }
        if (seconds > 0) {
            timer.innerHTML = seconds;
            seconds--;
        } else {
            timeIsOver();
        }
    }

    function checkIfAnswerIsRightOrWrong(result) {
        if (result === RIGHT) {
            pointsWon();
        } else {
            pointsLost();
        }
    }

    function setScore(score, questionsSolved, totalNum) {
        score.innerHTML = questionsSolved + "/" + totalNum;
    }

    function feedback(result, score, questionsSolved, totalNum, correctAnswer) {
        setScore(score, questionsSolved, totalNum);

        if (questionsSolved === totalNum) {
            done = WON;
            pointsWon();
            showResult(done, WON_FEEDBACK);
        } else {
            done = result;
            checkIfAnswerIsRightOrWrong(done);
            showResult(done, correctAnswer);
        }
    }

    function setupQuestionController() {
        question = new App.QuestionController(feedback);
        question.init();
    }

    function setupTimer() {
        timer = document.querySelector("#time");
        timer.innerHTML = seconds;
        setTime();
        interval = setInterval(setTime, 1000);
    }

    function setupResultField() {
        resultField = document.querySelector("#result");
        resultField.addEventListener("click", nextQuestion, false);
    }

    function setupMessage() {
        message = $("#game-message");
        message.css({
            top: 150,
            right: 45
        });
    }

    function getPoints() {
        return points;
    }

    function init() {
        points = 0;
        seconds = 10;
        playing = true;
        document.body.style.backgroundImage = "url('res/drawables/106405.jpg')";
        setupMessage();
        setupResultField();
        setupTimer();
        setupQuestionController();
    }

    return {
        init: init,
        getPoints: getPoints
    };
}());
