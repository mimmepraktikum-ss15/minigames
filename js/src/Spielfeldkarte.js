var App = App || {};
App.Spielfeldkarte = (function () {
    "use strict";
    /* eslint-env browser, jquery, fabric  */
    var canvas,
        playerName,
        playerString,
        playerNameField,
        canvasBackground,
        levelPoint,
        levelPoints = [],
        NUM_LEVELS = 25,
        xOne,
        yOne,
        xTwo,
        yTwo,
        line,
        i,
        startGameButton,
        snoopyImg,
        lineArray = [],
        matchesWon = 0,
        countHeartsDown = 0,
        extraHeart = false,
        gameFeatures,
        hearts,
        numLevels,
        numWortbilder,
        playerScore,
        points,
        gamePoints,
        numSnake,
        snoopyMoveLeft,
        snoopyMoveTop,
        playerJSON,
        SNOOPY_MOVE_DELAY = 2000,
        snoopyImage = "res/drawables/snoopy.png",
        LOGOUT = "Logout",
        DESCRIPTION = "Erklärung",
        heartImage = "url(res/drawables/heart.png)";

    function initCanvas() {
        canvas = new fabric.Canvas("game-map-canvas");
        canvas.setWidth(1200);
        canvas.setHeight(800);
        canvasBackground = "res/drawables/castle.png";
        canvas.setBackgroundImage(canvasBackground, canvas.renderAll.bind(canvas), {
            width: canvas.width,
            height: canvas.height
        });
    }

    function createLevelPoint(i) {
        var levelPoint,
            num;

        if (i >= 13 && i < 20) {
            num = i - 13;
            levelPoint = new fabric.Circle({
                radius: 20,
                fill: "#6E7B8B",
                top: num * 80 + 200,
                left: 50 * (Math.sin(num * 30)) + 1050
            });
        } else if (i >= 20) {
            num = i - 20;
            levelPoint = new fabric.Circle({
                radius: 20,
                fill: "#6E7B8B",
                left: 1150 - (num * 80 + 200),
                top: (50 * (Math.sin(num * 30)) + 710)
            });
        } else {
            levelPoint = new fabric.Circle({
                radius: 20,
                fill: "#6E7B8B",
                left: i * 80 + 80,
                top: 50 * (Math.sin(i * 20)) + 100
            });
        }
        levelPoint.selectable = false;
        return levelPoint;
    }

    function initLevelPoints() {
        for (i = 0; i < NUM_LEVELS; i++) {
            levelPoint = createLevelPoint(i);
            levelPoints.push(levelPoint);
            canvas.add(levelPoint);
        }
    }

    function makeLine(coords) {
        var line = new fabric.Line(coords, {
                stroke: "black",
                strokeWidth: 5,
                selectable: false
        });
        line.setVisible(false);
        canvas.add(line);
        line.sendBackwards(true);
        line.sendBackwards(true);
        lineArray.push(line);
    }

    function initPath() {
        for (i = 0; i < levelPoints.length - 1; i++) {
            xOne = levelPoints[i].getLeft() + levelPoints[i].getRadiusX();
            yOne = levelPoints[i].getTop() + levelPoints[i].getRadiusY();
            xTwo = levelPoints[i + 1].getLeft() + levelPoints[i + 1].getRadiusX();
            yTwo = levelPoints[i + 1].getTop() + levelPoints[i + 1].getRadiusY();
            line = makeLine([ xOne, yOne, xTwo, yTwo ]);
        }
    }

    function initSnoopy() {
        fabric.Image.fromURL(snoopyImage, function (snoopy) {
            snoopy.left = levelPoints[0].getLeft() - snoopy.width / 2;
            snoopy.top = levelPoints[0].getTop() - snoopy.height / 2;
            snoopy.selectable = false;
            canvas.add(snoopy);
            snoopyImg = snoopy;
        });
    }

    function drawLine(line) {
        line.setVisible(true);
        canvas.renderAll();
    }

    function removeLine() {
        line.setVisible(false);
        canvas.renderAll();
    }

    function moveSnoopy() {
        snoopyMoveLeft = levelPoints[matchesWon].getLeft() - snoopyImg.width / 2;
        snoopyMoveTop = levelPoints[matchesWon].getTop() - snoopyImg.height / 2;
        snoopyImg.animate("top", snoopyMoveTop, {
                onChange: canvas.renderAll.bind(canvas),
                duration: 200
            }).animate("left", snoopyMoveLeft, {
                onChange: canvas.renderAll.bind(canvas),
                duration: 200
            });
        canvas.renderAll();
    }

    function moveSnoopyForward(gameLost) {
        if (matchesWon >= levelPoints.length - 1) {
            App.SpielfeldController.startGameWon();
        } else {
            if (gameLost === true) {
                matchesWon--;
                line = lineArray[matchesWon];
                setTimeout(moveSnoopy, SNOOPY_MOVE_DELAY);
                setTimeout(removeLine, SNOOPY_MOVE_DELAY);
            } else {
                matchesWon++;
                line = lineArray[matchesWon - 1];
                setTimeout(drawLine(line), SNOOPY_MOVE_DELAY);
                setTimeout(moveSnoopy, SNOOPY_MOVE_DELAY);
            }
        }
    }

    function initGameDescription() {
        var description = document.querySelector("#game-description");
        description.innerHTML = DESCRIPTION;
        description.addEventListener("click", App.Description.init, false);
    }

    function onLogoutButtonClicked() {
        location.reload();
    }

    function initLogoutButton() {
        var logout = document.querySelector("#logout");
        logout.innerHTML = LOGOUT;
        logout.addEventListener("click", onLogoutButtonClicked, false);
    }

    function initPlayerName() {
        playerNameField = document.createElement("span");
        playerNameField.classList.add("playerName");
        playerNameField.innerHTML = playerName.toUpperCase();
        gameFeatures.appendChild(playerNameField);
    }

    function initButton() {
        startGameButton = document.createElement("input");
        startGameButton.classList.add("game-map-startNextGame");
        startGameButton.setAttribute("type", "submit");
        startGameButton.setAttribute("value", "Start");
        startGameButton.addEventListener("click", App.SpielfeldController.onStartGameButtonClicked, false);
        gameFeatures.appendChild(startGameButton);
    }

    function initHearts(hearts) {
        for (var i = 0; i < hearts; i++) {
            var heart = document.createElement("span");
            heart.classList.add("heart");
            heart.style.backgroundImage = heartImage;
            gameFeatures.appendChild(heart);
        }
    }

    function removeHearts() {
        gameFeatures.childNodes[playerJSON.hearts + 1].remove();
        countHeartsDown++;
    }

    function getJSONHearts() {
        return playerJSON.hearts - 1;
    }

    function getJSONLevels() {
        return playerJSON.numLevels;
    }

    function addHearts() {
        var heart = document.createElement("span");
        heart.classList.add("heart");
        heart.style.backgroundImage = "url(res/drawables/heart.png)";
        gameFeatures.appendChild(heart);
    }

    function updatePoints(gameLost) {
        if (gameLost === true) {
            gamePoints.innerHTML = 0;
        } else {
            gamePoints.innerHTML = playerJSON.points;
        }
    }

    function setupNewPlayer() {
        hearts = 5;
        numLevels = 0;
        numWortbilder = 0;
        points = 0;
        numSnake = 0;
        playerScore = 0;
        playerJSON = {
            "hearts": hearts,
            "numLevels": numLevels,
            "numWortbilder": numWortbilder,
            "points": points,
            "numSnake": numSnake,
            "playerScore": playerScore
        };

        playerString = JSON.stringify(playerJSON);
        localStorage.setItem(playerName, playerString);
    }

    function updateGameMap() {
        playerString = localStorage.getItem(playerName);
        playerJSON = JSON.parse(playerString);
        hearts = playerJSON.hearts;
        numLevels = playerJSON.numLevels;
        points = playerJSON.points;

        for (var i = 0; i <= numLevels - 1; i++) {
            moveSnoopyForward();
        }
    }

    function initPlayer() {
        playerName = App.Start.getPlayerName();
        if (localStorage.getItem(playerName) === null) {
            setupNewPlayer();
        } else {
            updateGameMap();
        }
        gamePoints = document.querySelector("#game-points");
        gamePoints.innerHTML = points;
    }

    function updateJSONLevels(gameLost) {
        if (gameLost === true) {
            playerJSON.numLevels = playerJSON.numLevels - 1;
        } else {
            playerJSON.numLevels = playerJSON.numLevels + 1;
        }
        playerString = JSON.stringify(playerJSON);
        localStorage.setItem(playerName, playerString);
    }

    function updateJSONHearts(gameLost) {
        if (gameLost === true) {
            playerJSON.hearts = 5;
        } else {
            if (extraHeart === false) {
                playerJSON.hearts = playerJSON.hearts - 1;
            } else {
                playerJSON.hearts = playerJSON.hearts + 1;
                extraHeart = false;
            }
        }
        playerString = JSON.stringify(playerJSON);
        localStorage.setItem(playerName, playerString);
    }

    function updateJSONPoints(points, gameLost) {
        if (gameLost === true) {
            playerJSON.points = points;
        } else {
            var allPoints = playerJSON.points + points;
            if (allPoints >= 100) {
                addHearts();
                extraHeart = true;
                updateJSONHearts(false);
                $("#hearts-message").html("<b> 100 </b> Punkte gewonnen! <br><b> 1 </b> Zusatzleben erspielt!").show().fadeOut(6000);
                playerJSON.points = allPoints - 100;
            } else {
                playerJSON.points = playerJSON.points + points;
            }
        }
        playerString = JSON.stringify(playerJSON);
        localStorage.setItem(playerName, playerString);
    }

    function updateJSONSnake() {
        playerJSON.numSnake = playerJSON.numSnake + 1;
        playerString = JSON.stringify(playerJSON);
        localStorage.setItem(playerName, playerString);
    }

    function updateJSONWortbilder() {
        playerJSON.numWortbilder = playerJSON.numWortbilder + 1;
        playerString = JSON.stringify(playerJSON);
        localStorage.setItem(playerName, playerString);
    }

    function updateJSONPlayerScore(collectAllPoints) {
        playerJSON.playerScore = playerJSON.playerScore + collectAllPoints;
        playerString = JSON.stringify(playerJSON);
        localStorage.setItem(playerName, playerString);
    }

    function setupHeader() {
        var header = document.querySelector("header");
        var headerImage = document.querySelector("#header-image");
        header.style.display = "block";
        headerImage.style.backgroundImage = "url('res/drawables/minigames.png')";
    }

    function setupGameMap() {
        var game = document.querySelector("#game");
        game.style.display = "block";
    }

    function checkKey(event) {
        var enter = event.keyCode;
        if (enter === 13) {
            document.onkeydown = null;
            App.SpielfeldController.onStartGameButtonClicked();
        }
    }

    function initView() {
        gameFeatures = document.querySelector("#game-map-game-features");
        gameFeatures.style.display = "block";

        initPlayerName();
        initButton();
        initHearts(playerJSON.hearts);
        initLogoutButton();
        initGameDescription();
    }

    function initKeyboard() {
        document.onkeydown = checkKey;
    }

    function init() {
        document.body.style.backgroundImage = "url('res/drawables/377769.png')";
        setupGameMap();
        setupHeader();
        initCanvas();
        initLevelPoints();
        initPath();
        initSnoopy();
        initPlayer();
        initView();
        document.onkeydown = checkKey;
    }

    return {
        init: init,
        initHearts: initHearts,
        initKeyboard: initKeyboard,
        getJSONHearts: getJSONHearts,
        getJSONLevels: getJSONLevels,
        moveSnoopyForward: moveSnoopyForward,
        removeHearts: removeHearts,
        updateJSONLevels: updateJSONLevels,
        updateJSONHearts: updateJSONHearts,
        updateJSONPoints: updateJSONPoints,
        updateJSONSnake: updateJSONSnake,
        updateJSONWortbilder: updateJSONWortbilder,
        updatePoints: updatePoints,
        updateJSONPlayerScore: updateJSONPlayerScore
    };
}());
