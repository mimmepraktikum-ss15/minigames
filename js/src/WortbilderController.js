var App = App || {};
App.WortbilderController = (function () {
    "use strict";

    /* eslint-env browser, jquery, fabric  */

    var answerInput,
        time,
        score,
        playing = true,
        wordsSolved = 0,
        numWords,
        numWordsActual,
        numPlays = 0,
        NUM_HEARTS_EASY = 5,
        NUM_HEARTS_MEDIUM = 4,
        NUM_HEARTS_DIFFICULT = 2,
        NUM_GAMES_TO_CHANGE_TO_MEDIUM = 3,
        NUM_GAMES_TO_CHANGE_TO_DIFFICULT = 5,
        numHearts,
        enterGuessInputField,
        enterGuessButton,
        view,
        ENTER_KEY = 13,
        countRemoveHearts = 0,
        timeInterval,
        guess,
        addListenerControl,
        seconds = 16;

    //stop the game
    //prepare the game for next time
    function stopGame() {
        clearInterval(timeInterval);
        wordsSolved = 0;
        playing = true;
        seconds = 16;
        countRemoveHearts = 0;
        App.Wortbilder.stopAnimateLetter();
    }

    function checkTest(result) {
        var test = App.SpielfeldController.getTest();
        if (test === true) {
            var gameContainer = document.querySelector("#game-container");
            gameContainer.style.display = "none";
            App.Description.init();
        } else {
            App.SpielfeldController.gameFinished(result);
        }
    }

    //game is won and will be closed
    function stopGameWon() {
        App.Wortbilder.showResult(true);
        stopGame();
        setTimeout(checkTest, 2000, true);
    }

    //game is lost and will be closed
    function stopGameLost() {
        App.Wortbilder.showResult(false);
        stopGame();
        setTimeout(checkTest, 2000, false);
    }

    //resets seconds/time
    //calls App.Wortbilder.wordToCanvas
    //sets playing = true; time is running
    function setupWord() {
        seconds = 16;
        App.Wortbilder.wordToCanvas();
        playing = true;
    }

    //check if game is won, because number of correct guess words, is reached
    //setup next word or stop the game
    function loadNextWord() {
        numWordsActual--;
        if (numWordsActual > 0) {
            addEventListener();
            playing = true;
            setupWord();
        } else {
            removeEventListener();
            stopGameWon();
        }
    }

    //update scoreview, sets wordSolved on the actual number
    function updateScore() {
        wordsSolved++;
        score.innerHTML = wordsSolved + "/" + numWords + " Wörter erraten";
    }

    //show and hide response of guess
    function showResponse(statusWordInput, listenerControl) {
        App.Wortbilder.showResponse(statusWordInput, seconds);
        setTimeout(App.Wortbilder.hideResponse, 1000, listenerControl);
    }

    /*
    called when the user enters a guess
    check if the guess is correct
    showResponse depends on correct/false
    update the view( score, hearts)
    reset input field
    */
    function onEnterGuessButtonClicked() {
        playing = false;
        answerInput = enterGuessInputField.value.toString();
        if (App.Wortbilder.checkUserGuess(answerInput) === true) {
            guess = true;
            addListenerControl = false;
            App.Wortbilder.stopAnimateLetter();
            showResponse(guess, addListenerControl);
            updateScore();
            enterGuessInputField.placeholder = "Deine Antwort";
            enterGuessInputField.value = "";
            setTimeout(loadNextWord, 1500);
        } else {
            App.Wortbilder.reduceHearts(numHearts);
            if (countRemoveHearts >= numHearts - 1) {
                guess = false;
                addListenerControl = false;
                App.Wortbilder.stopAnimateLetter();
                showResponse(guess, addListenerControl);
                stopGameLost();
            } else {
                guess = false;
                addListenerControl = true;
                showResponse(guess, addListenerControl);
                enterGuessInputField.placeholder = "Deine Antwort";
                enterGuessInputField.value = "";
                countRemoveHearts++;
            }
        }
    }

    //removes EventListener from enterGuessButton and enterGuessInputField
    function removeEventListener() {
        enterGuessButton.removeEventListener("click", onEnterGuessButtonClicked, false);
        $("#answer-wortbilder").unbind("keypress");
    }

    //add EventListener to enterGuessButton and enterGuessInputField
    function addEventListener() {
        enterGuessInputField.focus();
        $(enterGuessInputField).keypress(function(e) {
            var key = e.which || e.keyCode;
            if(key === ENTER_KEY) {
                onEnterGuessButtonClicked();
            }
        });
        enterGuessButton.addEventListener("click", onEnterGuessButtonClicked, false);
    }

    //set playing = true
    //time is running again
    function setPlayingTrue() {
        playing = true;
    }

    //setup input-field
    function setupEnterGuessInputField() {
        enterGuessInputField = document.getElementById("answer-wortbilder");
        enterGuessInputField.style.backgroundColor = "#f0e68c";
    }

    //setup Guess-Button
    function setupGuessButton() {
        enterGuessButton = document.getElementsByName("wortbilder-enter-guess")[0];
        enterGuessButton.backgroundColor = "#900";
    }

    //get number of words
    function getNumberOfWords() {
        numWords = 5;
        numWordsActual = numWords;
        return numWords;
    }

    //setups the score view
    function setupScore() {
        view = document.querySelector("#wortbilder-score");
        score = document.createElement("span");
        score.classList.add("wortbilder-score");
        numWords = getNumberOfWords();
        score.innerHTML = wordsSolved + "/" + numWords + " Wörter erraten";
        view.appendChild(score);
    }

    //setup and coordinates the time
    function setTime() {
        time = document.querySelector("#wortbilder-time");
        if (!playing) {
            return;
        }
        if (seconds > 1) {
            seconds--;
            time.innerHTML = seconds;
        } else {
            App.Wortbilder.stopAnimateLetter();
            if (countRemoveHearts >= numHearts - 1) {
                removeEventListener();
                playing = false;
                seconds--;
                time.innerHTML = seconds;
                App.Wortbilder.stopAnimateLetter();
                App.Wortbilder.reduceHearts(numHearts);
                stopGameLost();
            } else {
                playing = false;
                seconds--;
                time.innerHTML = seconds;
                App.Wortbilder.reduceHearts(numHearts);
                App.Wortbilder.pointsLost();
                App.Wortbilder.showMessage();
                countRemoveHearts++;
                App.Wortbilder.showGuessedWord();
                seconds = 15;
                time.innerHTML = seconds;
                clearInterval(timeInterval);
                setTimeout(setupWord, 3000);
                timeInterval = setInterval(setTime, 1000);
            }
        }
    }

    //chose number of heatrs/lives. it depends on
    //how often the player played this game
    function getNumberOfHearts() {
        numPlays++;
        if (numPlays <= NUM_GAMES_TO_CHANGE_TO_MEDIUM) {
            numHearts = NUM_HEARTS_EASY;
        } else if (numPlays > NUM_GAMES_TO_CHANGE_TO_MEDIUM && numPlays <= NUM_GAMES_TO_CHANGE_TO_DIFFICULT) {
            numHearts = NUM_HEARTS_MEDIUM;
        } else {
            numHearts = NUM_HEARTS_DIFFICULT;
        }
        App.Wortbilder.setupHearts(numHearts);
    }

    function init() {
        setupWord();
        getNumberOfHearts();
        setupGuessButton();
        setupEnterGuessInputField();
        addEventListener();
        setupScore();
        timeInterval = setInterval(setTime, 1000);
    }

    return {
        init: init,
        removeEventListener: removeEventListener,
        addEventListener: addEventListener,
        setPlayingTrue: setPlayingTrue
    };
}());
