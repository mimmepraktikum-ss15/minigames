var App = App || {};
App.AddHighscore = (function () {
    "use strict";
    /* eslint-env browser, jquery, fabric  */

    var highscoreButton,
        closeHighscoreButton,
        highscoreTable,
        highscoreView,
        actualRow,
        rankCell,
        userCell,
        scoreCell,
        rank = 1,
        highscoreJSON,
        highscoreString,
        actualPlayerName,
        actualPlayerScore,
        newHighscoreJSON,
        newHighscoreString,
        tenHighestScores = [],
        tenHighestNames = [],
        NUM_MAX_HIGHSCORE = 10,
        HIGHSCORE = "Highscore";

    //Highscore is now unvisible
    function onCloseHighscoreButtonClicked() {
        highscoreView.style.display = "none";
    }

    //inits the "Schließen"-Button on Highscore and adds a onClickListener to it
    function initCloseHighscoreButton() {
        closeHighscoreButton = document.getElementsByName("closeHighscore")[0];
        closeHighscoreButton.addEventListener("click", onCloseHighscoreButtonClicked, false);
    }

    //Highscore is now visible
    //adds a ClickListener to Highscore
    function onShowHighscoreButtonClicked(highscoreViewId) {
        highscoreView = document.getElementById(highscoreViewId);
        highscoreView.style.display = "block";
        highscoreView.addEventListener("click", onCloseHighscoreButtonClicked, false);
    }

    //init the new JSONHighscore list or adds new Data to it
    //Only the ten best player will be saved in the Highscore, the other will be removed
    //To save memory capacity
    function addToNewJSON(highScore, highName) {
        if (newHighscoreJSON === undefined) {
            newHighscoreJSON = {
                "DATA": [
                    {"playerName": highName,
                    "playerScore": highScore}
                ]
            };
        } else {
            newHighscoreJSON.DATA.push({"playerName": highName, "playerScore": highScore});
        }
    }

    //overwrites the old localStorage-HighscoreList with the new one
    function updateHighscoreJSON() {
        newHighscoreString = JSON.stringify(newHighscoreJSON);
        localStorage.setItem(HIGHSCORE, newHighscoreString);
        newHighscoreJSON = undefined;
    }

    //inits the Highscore-Button on the page and adds a onClickListener to it
    //it depends if the Button is on Start.js or on GameWon.js
    function initShowHighscoreButton(showHighscoreButtonId, highscoreViewId) {
        highscoreButton = document.getElementsByName(showHighscoreButtonId)[0];
        highscoreButton.addEventListener("click", function () {
            onShowHighscoreButtonClicked(highscoreViewId);
        }, false);
    }

    /*setup the Highscore-Table.
    adds only the ten best player to the table.
    if there aren't so many players yet, the table is filled with "Users" who gained
    0 Points.
    If there are more than ten Players, who won the game, the others will be removed
    from the Highscore*/
    function initHighscoreTable(tableId) {
        var allScores = [],
            allNames = [];
        highscoreString = localStorage.getItem(HIGHSCORE);
        highscoreJSON = JSON.parse(highscoreString);
        tenHighestScores = [];
        tenHighestNames = [];
        highscoreTable = document.getElementById(tableId);
        for (var i = 0; i < highscoreJSON.DATA.length; i++) {
            allScores[i] = highscoreJSON.DATA[i].playerScore;
            allNames[i] = highscoreJSON.DATA[i].playerName;
        }
        for(i = 0; i < NUM_MAX_HIGHSCORE; i++) {
            tenHighestScores[i] = Math.max.apply(Math, allScores);
            tenHighestNames[i] = allNames[allScores.indexOf(tenHighestScores[i])];
            addToNewJSON(tenHighestScores[i], tenHighestNames[i]);
            if(tenHighestNames[i] === undefined){
               tenHighestNames[i] = "User";
                tenHighestScores[i] = 0;
            }
            allNames.splice(allScores.indexOf(tenHighestScores[i]), 1);
            allScores.splice(allScores.indexOf(tenHighestScores[i]), 1);
        }
        updateHighscoreJSON();
        for (var k = 0; k < tenHighestNames.length; k++) {
            rank = k + 1;
            actualPlayerName = tenHighestNames[k];
            actualPlayerScore = tenHighestScores[k];
            actualRow = highscoreTable.insertRow(rank);
            rankCell = actualRow.insertCell(0);
            rankCell.innerHTML = rank;
            userCell = actualRow.insertCell(1);
            userCell.innerHTML = actualPlayerName;
            scoreCell = actualRow.insertCell(2);
            scoreCell.innerHTML = actualPlayerScore;
        }
    }

    return {
        initHighscoreTable: initHighscoreTable,
        initCloseHighscoreButton: initCloseHighscoreButton,
        initShowHighscoreButton: initShowHighscoreButton
    };
}());
