var App = App || {};
App.HangmanGame = function () {
    "use strict";
    /* eslint-env browser, fabric */
    var wordlist,
        word,
        wordWithPlaceholders,
        wordNumber,
        rightGuess,
        TRIES,
        PLACEHOLDER = "_";

    function myWord() {
        wordlist = App.Arrays.getListDifficult();
        wordNumber = fabric.util.getRandomInt(0, wordlist.length - 1);
        word = wordlist[wordNumber].toUpperCase();
        TRIES = 8;
    }

    function placeholderWord() {
        wordWithPlaceholders = word;
        for (var i = 0; i < word.length; i++) {
            wordWithPlaceholders = wordWithPlaceholders.replace(wordWithPlaceholders.charAt(i), PLACEHOLDER);
        }
    }

    function getPlaceholders(letter) {
		for (var i = 0; i < word.length; i++) {
			if (letter === word.charAt(i)) {
				wordWithPlaceholders = wordWithPlaceholders.substring(0, i) + letter + wordWithPlaceholders.substring(i + 1);
			}
		}
		return wordWithPlaceholders;
	}

    function getHangmanWord() {
        return word;
    }

    //Wird in Hangman.js aufgerufen
    function guess(letter) {
        for (var i = 0; i < word.length; i++) {
			rightGuess = word.indexOf(letter);
        }
		if (rightGuess >= 0) {
			return true;
		} else {
			TRIES--;
			return false;
		}
    }

    //Wird in Hangman.js aufgerufen
    function isGameWon() {
        if (wordWithPlaceholders.indexOf(PLACEHOLDER) === -1) {
			return true;
		} else {
			return false;
		}
    }

    //Wird in Hangman.js aufgerufen
    function isGameOver() {
        if (TRIES === 0){
			return true;
		} else {
			return false;
		}
    }

    function getGuessesLeft() {
        return TRIES;
    }

    return {
        myWord: myWord,
        placeholderWord: placeholderWord,
        getPlaceholders: getPlaceholders,
        getHangmanWord: getHangmanWord,
        guess: guess,
        getGuessesLeft: getGuessesLeft,
        isGameWon: isGameWon,
        isGameOver: isGameOver
    };
};
