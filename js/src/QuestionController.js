var App = App || {};
App.QuestionController = function (feedback) {
    "use strict";
    /* eslint-env browser, fabric, jquery */
    var question,
        questionField,
        answer,
        answerField,
        result,
        correctAnswer,
        score,
        jsonObject,
        randomQuestions,
        randomHearts,
        totalNum,
        view,
        MAX = 2,
        countHeartsDown,
        questionsSolved,
        callback = feedback,
        answerArray = [],
        RIGHT = "richtig",
        FALSE = "falsch",
        LOST = "verloren",
        LOST_FEEDBACK = "Das Spiel ist verloren. Alle Leben sind verbraucht.",
        URL = "js/json/JSON_Object_True_False.json",
        heartImage = "url(res/drawables/heart.png)",
        snoopyImage = "url('res/drawables/snoopy2.png')";

    function reduceHearts() {
        view.childNodes[randomHearts - countHeartsDown].remove();
        countHeartsDown++;
    }

    function removeEventListener() {
        for (var i = 0; i < MAX; i++) {
            answerField.childNodes[i].removeEventListener("click", onAnswerClicked, false);
        }
    }

    function wrong() {
        reduceHearts();
        if(countHeartsDown === randomHearts) {
            result = LOST;
            correctAnswer = LOST_FEEDBACK;
        } else {
            result = FALSE;
        }
    }

    function right() {
        questionsSolved++;
        result = RIGHT;
    }

    function onAnswerClicked(event) {
        question.innerHTML = "";
        questionField.classList.add("hidden");
        correctAnswer = jsonObject[randomQuestions].correct_answer;
        removeEventListener();
        if (event.target.innerHTML.toLowerCase() === jsonObject[randomQuestions].correct) {
            right();
        } else {
            wrong();
        }
        callback(result, score, questionsSolved, totalNum, correctAnswer);
    }

    function addEventListener() {
        for (var i = 0; i < MAX; i++) {
            answerField.childNodes[i].addEventListener("click", onAnswerClicked, false);
        }
    }

    function checkIfQuestionAlreadyExists() {
        if($.inArray(randomQuestions, answerArray) !== -1) {
            while ($.inArray(randomQuestions, answerArray) !== -1) {
                randomQuestions = fabric.util.getRandomInt(0, 99);
            }
        }
    }

    function setQuestion() {
        randomQuestions = fabric.util.getRandomInt(0, 99);
        checkIfQuestionAlreadyExists();
        answerArray.push(randomQuestions);
        question.innerHTML = jsonObject[randomQuestions].question;
        questionField.classList.remove("hidden");
    }

    function createAnswers() {
        for (var i = 1; i <= MAX; i++) {
            answer = document.createElement("span");
            answer.classList.add("answer" + i);
            answer.innerHTML = jsonObject[0].options[i - 1].toUpperCase();
            answer.addEventListener("click", onAnswerClicked, false);
            answerField.appendChild(answer);
        }
    }

    function createQuestion() {
        question = document.createElement("span");
        question.classList.add("question");
        setQuestion();
        questionField.appendChild(question);
    }

    function processServerResponse(data) {
        jsonObject = data;
        createQuestion();
        createAnswers();
    }

    function getJSON() {
        $.ajax({
            type: "GET",
            url: URL,
            dataType: "json",
            success: processServerResponse
        });
    }

    function setupAnswerField() {
        answerField = document.querySelector("#answers");
    }

    function setupQuestionField() {
        questionField = document.querySelector("#questionField");
    }

    function setupHearts() {
        for (var i = 0; i < randomHearts; i++) {
            var heart = document.createElement("span");
            heart.classList.add("heart");
            heart.style.backgroundImage = heartImage;
            view.appendChild(heart);
        }
    }

    function setupScore() {
        score = document.createElement("span");
        score.classList.add("score");
        score.innerHTML = questionsSolved + "/" + totalNum;
        view.appendChild(score);
    }

    function setupView() {
        view = document.querySelector("#view");
    }

    function setupSnoopy() {
        var snoopy = document.querySelector("#snoopy");
        snoopy.style.backgroundImage = snoopyImage;
    }

    function getNumberOfHearts() {
        randomHearts = fabric.util.getRandomInt(3, 5);
    }

    function getNumberOfQuestions() {
        totalNum = fabric.util.getRandomInt(7, 10);
    }

    function getCountHeartsDown() {
        return countHeartsDown;
    }

    function getRandomHearts() {
        return randomHearts;
    }

    function getQuestionField() {
        return questionField;
    }

    function init() {
        countHeartsDown = 0;
        questionsSolved = 0;
        getNumberOfQuestions();
        getNumberOfHearts();
        setupSnoopy();
        setupView();
        setupScore();
        setupHearts();
        setupQuestionField();
        setupAnswerField();
        getJSON();
    }

    return {
        init: init,
        getCountHeartsDown: getCountHeartsDown,
        getRandomHearts: getRandomHearts,
        setQuestion: setQuestion,
        addEventListener: addEventListener,
        getQuestionField: getQuestionField,
        reduceHearts: reduceHearts,
        removeEventListener: removeEventListener
    };
};
