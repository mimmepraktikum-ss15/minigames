var App = App || {};
App.Description = (function () {
    "use strict";
    /* eslint-env browser, jquery, fabric  */
    var game,
        description,
        screen,
        minigameDescription,
        playingGame,
        playingHTML,
        testButton,
        gameContainer,
        minigame,
        NUM = 6,
        TITLE = "MINIGAMES",
        BACK = "Zurück",
        TEST = "Spiel testen",
        minigames = [],
        mapMinigamesDescription = {},
        mapMinigamesScreen = {},
        mapMinigamesHTML = {},
        mapMinigamesTest = {};

    function onBackButtonClicked() {
        game.style.display = "block";
        description.style.display = "none";
        minigameDescription.style.display = "none";
        screen.style.display = "none";
        testButton.style.display = "none";
        App.Spielfeldkarte.initKeyboard();
    }

    function onGameLoad() {
        gameContainer = document.querySelector("#game-container");
        gameContainer.style.display = "block";
        playingGame.init();
    }

    function onTestButtonClicked() {
        App.SpielfeldController.test(true);
        description.style.display = "none";
        gameContainer = $("#game-container").load(playingHTML, onGameLoad);
    }

    function setupTitle() {
        var title = document.querySelector("#title");
        title.innerHTML = "";
        title.innerHTML = event.target.innerHTML;
    }

    function setupText() {
        var text = document.querySelector("#text");
        text.innerHTML = "";
        text.innerHTML = mapMinigamesDescription[event.target.innerHTML];
    }

    function setupScreen() {
        if (event.target.innerHTML !== minigames[0]) {
            screen.style.display = "block";
            screen.style.backgroundImage = "";
            screen.style.backgroundImage = mapMinigamesScreen[event.target.innerHTML];
        } else {
            screen.style.display = "none";
        }
    }

    function setupTestButton() {
        if (event.target.innerHTML !== minigames[0]) {
            testButton.style.display = "block";
            testButton.innerHTML = TEST;
            testButton.addEventListener("click", onTestButtonClicked, false);
        } else {
            testButton.style.display = "none";
        }
    }

    function onMinigameClicked(event) {
        minigameDescription.style.display = "block";
        setupTitle();
        setupText();
        setupScreen();
        setupTestButton();
        playingGame = mapMinigamesTest[event.target.innerHTML];
        playingHTML = mapMinigamesHTML[event.target.innerHTML];
    }

    function setupMinigamesTitle() {
        var minigamesTitle = document.querySelector("#minigames");
        minigamesTitle.innerHTML = TITLE;
    }

    function setupList() {
        for (var i = 0; i <= NUM; i++) {
            minigame = document.querySelector("#minigame" + i);
            minigame.innerHTML = minigames[i];
            minigame.addEventListener("click", onMinigameClicked, false);
        }
    }

    function setupBackButton() {
        var backButton = document.querySelector("#back-button");
        backButton.innerHTML = BACK;
        backButton.addEventListener("click", onBackButtonClicked, false);
    }

    function onGameDescriptionButtonClicked() {
        game.style.display = "none";
        description.style.display = "block";
        setupMinigamesTitle();
        setupList();
        setupBackButton();
    }

    function setupMaps() {
        minigames = App.Arrays.getArrayMinigames();
        mapMinigamesDescription = App.Arrays.getMapMinigamesDescription();
        mapMinigamesScreen = App.Arrays.getMapMinigamesScreen();
        mapMinigamesHTML = App.Arrays.getMapMinigamesHTML();
        mapMinigamesTest = App.Arrays.getMapMinigamesTest();
    }

    function setupView() {
        game = document.querySelector("#game");
        description = document.querySelector("#description-site");
        minigameDescription = document.querySelector("#minigame-description");
        screen = document.querySelector("#screen");
        testButton = document.querySelector("#test-button");
    }

    function init() {
        document.onkeydown = null;
        document.body.style.backgroundImage = "url('res/drawables/377769.png')";
        setupView();
        setupMaps();
        onGameDescriptionButtonClicked();
    }

    return {
        init: init
    };
}());
