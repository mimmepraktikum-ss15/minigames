var App = App || {};
App.Wortbilder = (function () {
    "use strict";

      /* eslint-env browser, jquery, fabric  */

    var canvas,
        points,
        message,
        countHeartsDown,
        view,
        canvasBackground,
        randomNumber,
        actualWord,
        wordInWordlist,
        actualLetter,
        letter,
        guessWord,
        statusInput,
        statusInputResponse,
        statusInputResponseColor,
        animateLetterInterval,
        ResultToShow,
        RESULT,
        RESULT_BEGIN = "Du hast ",
        RESULT_WON = "Gewonnen",
        RESULT_LOST = "Verloren",
        wortliste,
        wordsAlreadyUsed = [],
        numPlays,
        playerName,
        playerString,
        playerJSON,
        NUM_GAMES_TO_CHANGE_TO_MEDIUM = 3,
        NUM_GAMES_TO_CHANGE_TO_DIFFICULT = 5,
        i;

    //inits the Background of the Canvas
    function canvasSetBackground() {
        canvasBackground = "res/drawables/snoopy_wortbilder_2.jpg";
        canvas.setBackgroundImage(canvasBackground, canvas.renderAll.bind(canvas), {
            width: canvas.width,
            height: canvas.height
        });
    }

    //inits the Canvas
    function initCanvas() {
        canvas = new fabric.Canvas("wortbilder-canvas");
        canvas.setWidth(600);
        canvas.setHeight(450);
    }

    //clears the Canvas/ removes all Objects on the canvas and adds the Backgound to it
    function prepareCanvas() {
        canvas.clear();
        canvasSetBackground();
    }

    //removes one heart
    //called if the time is out or the player enters a wrong word
    function reduceHearts(numHearts) {
        view.childNodes[numHearts - countHeartsDown - 1].remove();
        countHeartsDown++;
    }

    //init/setup and show the hearts. they represent the players lives in this game
    function setupHearts(numHearts) {
        view = document.querySelector("#wortbilder-heartView");
        for (i = 0; i < numHearts; i++) {
            var heart = document.createElement("span");
            heart.classList.add("wortbilder-heart");
            heart.style.backgroundImage = "url(res/drawables/heart.png)";
            view.appendChild(heart);
        }
    }

    //letters on canvas don't move
    function stopAnimateLetter() {
        clearInterval(animateLetterInterval);
    }

    //letters on canvas are animated and move over the vanvas
    function animateLetter(currentLetter) {
        animateLetterInterval = setInterval(function () {
            currentLetter.animate("left", fabric.util.getRandomInt(40, 500), {
                onChange: canvas.renderAll.bind(canvas),
                duration: 1500,
                easing: fabric.util.ease.easeOutCirc
            });
            currentLetter.animate("top", fabric.util.getRandomInt(40, 410), {
                onChange: canvas.renderAll.bind(canvas),
                duration: 1500,
                easing: fabric.util.ease.easeOutCirc
            });
            canvas.renderAll();
        }, 1000);
    }

    // a random Word is choosen
    // in this game the word is never used again
    function chooseRandomWord() {
        randomNumber = fabric.util.getRandomInt(0, wortliste.length - 1);
        actualWord = wortliste[randomNumber].toUpperCase();
        wordInWordlist = wordsAlreadyUsed.indexOf(actualWord);
        if (wordInWordlist === -1) {
            wordsAlreadyUsed.push(actualWord);
            return actualWord;
        } else if (wordsAlreadyUsed.indexOf(actualWord) === -1 && wordsAlreadyUsed.length === wortliste.length) {
            wordsAlreadyUsed = [];
            chooseRandomWord();
        } else {
            chooseRandomWord();
        }
    }

    //brings the choosen word to canvas
    //seperates the letters
    function wordToCanvas() {
        prepareCanvas();
        chooseRandomWord();
        for (i = 0; i < actualWord.length; i++) {
            actualLetter = actualWord.charAt(i);
            letter = new fabric.Text(actualLetter, {
                left: fabric.util.getRandomInt(20, 350),
                top: fabric.util.getRandomInt(20, 250),
                fontSize: 50,
                fontWeight: "bold"
            });
            letter.selectable = false;
            canvas.add(letter);
            canvas.renderAll();
        }
        canvas.renderAll();
        canvas.forEachObject(function (obj) {
            animateLetter(obj);
        });
    }

    //shows the correct Answer
    function showGuessedWord() {
        prepareCanvas();
        guessWord = new fabric.Text(actualWord, {
            fontSize: 50,
            fontWeight: "bold"
        });
        guessWord.selectable = false;
        canvas.centerObject(guessWord);
        canvas.add(guessWord);
        canvas.renderAll();
    }

    //checks if user guess is the correct Answer
    function checkUserGuess(answerInput) {
        if (answerInput.toLowerCase() === actualWord.toLowerCase()) {
            return true;
        } else {
            return false;
        }
    }

    //choose how many points the user gained
    //depends on how fast the user enters the correct answer
    function pointsWon(seconds) {
        if (seconds >= 10) {
            points += 5;
        } else if (seconds >= 7) {
            points += 3;
        } else if (seconds >= 1) {
            points += 1;
        }
    }

    function pointsLost() {
        points -= 5;
    }

    function getPoints() {
        return points;
    }

    //shows how many points the users gained or lost
    function showMessage() {
        message.html("<b>" + points + "</b><br>Punkte").show().fadeOut(4000);
    }

    //show the response of the users guess
    //show how many points the user gained/lost
    function showResponse(statusWordInput, seconds) {
        App.WortbilderController.removeEventListener();
        if (statusWordInput === true) {
            pointsWon(seconds); //
            statusInput = "RICHTIG";
            statusInputResponseColor = "green";
        } else {
            pointsLost();
            statusInput = "FALSCH";
            statusInputResponseColor = "red";
        }
        statusInputResponse = new fabric.Text(statusInput, {
            fontSize: 50,
            fontWeight: "bold",
            fill: statusInputResponseColor
        });
        showMessage();
        statusInputResponse.selectable = false;
        canvas.centerObject(statusInputResponse);
        canvas.add(statusInputResponse);
    }

    //the response is unvisible
    function hideResponse(listenerControl) {
        message.finish();
        statusInputResponse.remove();
        canvas.renderAll();
        if (listenerControl === true){
            App.WortbilderController.addEventListener();
            App.WortbilderController.setPlayingTrue();
        }
    }

    //removes all objects on canvas
    function deleteCanvas() {
        canvas.clear();
    }

    //show result of the game, when the game is finished
    function showResult(result, seconds) {
        canvas.clear();
        stopAnimateLetter();
        prepareCanvas();
        if (result === true) {
            pointsWon(seconds);
            RESULT = RESULT_BEGIN + RESULT_WON;
        } else {
            pointsLost();
            RESULT = RESULT_BEGIN + RESULT_LOST;
        }
        ResultToShow = new fabric.Text(RESULT, {
            fontSize: 50,
            fontWeight: "bold"
        });
        ResultToShow.selectable = false;
        canvas.add(ResultToShow);
        canvas.centerObject(ResultToShow);
        canvas.renderAll();
        setTimeout(deleteCanvas, 2000);
    }

    //select the wortiste easy, medium or difficult
    //depends on how often the user plays the game
    function chooseWortliste() {
        playerName = App.Start.getPlayerName();
        playerString = localStorage.getItem(playerName);
        playerJSON = JSON.parse(playerString);
        numPlays = playerJSON.numWortbilder;
        if (numPlays <= NUM_GAMES_TO_CHANGE_TO_MEDIUM) {
            wortliste = App.Arrays.getListEasy();
        } else if (numPlays > NUM_GAMES_TO_CHANGE_TO_MEDIUM && numPlays <= NUM_GAMES_TO_CHANGE_TO_DIFFICULT) {
            wortliste = App.Arrays.getListMedium();
        } else {
            wortliste = App.Arrays.getListDifficult();
        }
    }

    //setup the message how many pints the user gained/lost
    function setupMessage() {
        message = $("#game-message");
        message.css({
            top: 170,
            right: 25
        });
    }

    //called if the game was chosen
    function init() {
        points = 0;
        countHeartsDown = 0;
        document.body.style.backgroundImage = "url('res/drawables/349141.jpg')";
        setupMessage();
        initCanvas();
        chooseWortliste();
        canvas.renderAll();
        App.WortbilderController.init();
    }

    return {
        init: init,
        reduceHearts: reduceHearts,
        showSolvedWord: showGuessedWord,
        wordToCanvas: wordToCanvas,
        showGuessedWord: showGuessedWord,
        setupHearts: setupHearts,
        checkUserGuess: checkUserGuess,
        showResponse: showResponse,
        hideResponse: hideResponse,
        showResult: showResult,
        stopAnimateLetter: stopAnimateLetter,
        deleteCanvas: deleteCanvas,
        getPoints: getPoints,
        pointsLost: pointsLost,
        showMessage: showMessage
    };
}());
