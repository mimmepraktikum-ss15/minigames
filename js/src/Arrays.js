var App = App || {};
App.Arrays = (function () {
    "use strict";
    /* eslint-env browser, fabric */

    var listEasy = [],
        listMedium = [],
        listDifficult = [],
        mapCities = {},
        mapPromis = {},
        cities = [],
        promis = [],
        minigames = [],
        minigamesDescription = {},
        minigamesScreen = {},
        minigamesHTML = {},
        minigamesTest = {},
        gameText,
        quizWahrFalschText,
        quiz4AnswersText,
        wortbilderText,
        bilderratenText,
        hangmanText,
        snakeText;

    function setupGameText() {
        gameText = "Nachdem sich der Spieler auf der Startseite eingeloggt hat, startet das Minigames-Spiel. Zu Beginn befindet sich der Spieler auf dem ersten Level der Spielfeldkarte, hat 5 Leben und 0 Punkte. Klickt er auf den Start-Button oder Enter, wird eines der insgesamt 6 Minispiele, die im Folgenden genauer beschrieben werden, per Zufall ausgewählt. <br><br>Gewinnt der Spieler das Minispiel, darf er auf der Spielfeldkarte ein Level nach vorne rücken. Verliert er es, wird ihm ein Leben abgezogen. <br><br>Während der Minispiele kann der Spieler Punkte sammeln. Hat er insgesamt 100 Punkte erreicht, bekommt er ein Leben gutgeschrieben. <br>Hat er alle seine Leben verloren, ist der Spieler 'game-over'. D.h. er muss auf der Spielfeldkarte ein Level zurückgehen, hat wieder 5 Leben und 0 Punkte.";
    }

    function setupQuizWahrFalschText() {
        quizWahrFalschText = "Bei dem Quiz-Spiel Wahr/Falsch werden dem Spieler Fragen gestellt, die er mit 'wahr' oder 'falsch' innerhalb einer bestimmten Zeit beantworten muss. <br><br>Je schneller er die Frage richtig beantwortet, desto mehr Punkte bekommt er (1, 3 oder maximal 5 Punkte). Weiß der Spieler die Antwort auf eine Frage nicht oder ist die Zeit abgelaufen, verliert er ein Leben und erhält 5 Minuspunkte. Nach jeder Antwort erscheint ein entsprechendes Feedback. <br><br>Hat der Spieler alle seine Leben verbraucht, hat er das Minispiel verloren. Hat der Spieler jedoch eine bestimmte Anzahl von Fragen richtig beantwortet, hat er das Spiel gewonnen.";
    }

    function setupQuiz4AnswersText() {
        quiz4AnswersText = "Bei diesem Quiz-Spiel stehen dem Spieler 4 Antwortmöglichkeiten zur Verfügung. <br><br>Je schneller er eine Frage richtig beantwortet, desto mehr Punkte bekommt er (1, 3 oder maximal 5 Punkte). Weiß der Spieler die Antwort auf eine Frage nicht oder ist die Zeit abgelaufen, verliert er ein Leben und erhält 5 Minuspunkte. Nach jeder Antwort erscheint ein entsprechendes Feedback. <br><br>Hat der Spieler alle seine Leben verbraucht, hat er das Minispiel verloren. Hat der Spieler jedoch eine bestimmte Anzahl von Fragen richtig beantwortet, hat er das Spiel gewonnen.";
    }

    function setupWortbilderText() {
        wortbilderText = "Bei dem Wortbilder-Spiel müssen die fliegenden Buchstaben zu einem bestimmten Wort zusammengesetzt werden. <br><br>Je schneller der Spieler ein Wort richtig errät, desto mehr Punkte bekommt er (1, 3 oder max. 5 Punkte). Ist das vermutete Wort falsch oder ist die Zeit abgelaufen wird ein Herz abgezogen und der Spieler erhält 5 Minuspunkte. <br><br>Sind alle Herzen verspielt, hat der Spieler das Minispiel verloren. Der Spieler hat gewonnen, wenn er eine bestimmte Anzahl von Wörtern, bevor die Herzen verspielt sind, richtig erraten hat.";
    }

    function setupBilderratenText() {
        bilderratenText = "Bei diesem Spiel muss der Spieler Städte oder Prominente erraten. Dabei werden die Bilder nach und nach immer weiter aufgedeckt, wobei die Promis etwas verpixelt sind. Glaubt der Spieler die Antwort zu wissen, muss er auf den Stopp-Button klicken (Zeit stoppt), das Ergebnis in das Eingabe-Feld eingeben und mit Enter bestätigen. Je schneller der Spieler ein Bild richtig errät, desto mehr Punkte bekommt er (1, 3 o. max. 5 Punkte). Liegt der Spieler falsch oder ist die Zeit abgelaufen, verliert er ein Leben und erhält 5 Minuspunkte. Sind alle Leben verbraucht, ist das Minispiel verloren. Hat der Spieler jedoch eine bestimmte Anzahl von Bildern richtig erraten, hat er das Spiel gewonnen. <br><b>WICHTIG:</b> Bei den Promis muss sowohl der Vorname als auch der Nachname eingegeben werden (Vorname/Nachname, in dieser Reihenfolge; ausgenommen: Künstlernamen wie RIHANNA).";
    }

    function setupHangmanText() {
        hangmanText = "Beim Hangman-Spiel muss der Spieler ein Wort erraten. Das gesuchte Wort wird mit Platzhaltern dargestellt (_). <br><br>Über die Tastatur gibt der Spieler einen gewünschten Buchstaben ein. Liegt er falsch, wird ein Körperteil von unserem Spielfeldmännchen Snoopy an den Galgen gezeichnet und ein Leben abgezogen. <br><br>Hat der Spieler alle seine Leben verbraucht, d.h. hängt Snoopy vollständig am Galgen (8 Körperteile), hat er das Minispiel verloren und erhält 5 Minuspunkte. Hat der Spieler jedoch das Wort richtig erraten, hat er das Spiel gewonnen und bekommt 5 Punkte auf seinem Punktekonto gutgeschrieben.";
    }

    function setupSnakeText() {
        snakeText = "Bei diesem Spiel handelt es sich um ein klassisches Snake-Spiel. <br><br>Die Schlange soll die Woodstocks essen und dabei den Wänden und den Büschen ausweichen. <br><br>Steuerbar ist die Schlange entweder über die Pfeiltasten oder über WASD. <br><br>Der Spieler hat verloren, wenn er an eine Wand oder einen Busch stößt (5 Minuspunkte). Das Spiel ist gewonnen, wenn alle Woodstocks gegessen wurden. Dann werden dem Spieler 5 Punkte gutgeschrieben.";
    }

    function initListEasy() {
        listEasy = ["Kaktus", "Paprika", "Blume", "Banane", "Minion", "Mais", "Birne", "Kiwi", "Quark", "Saft", "Traube", "Snoopy", "Katze", "Hund", "Thron", "Lampe", "Farbe", "Kind", "Motor", "Kuchen", "Geld", "Kerzen", "Wachs", "Biene", "Vogel", "Hase", "Haus", "Zimt", "Farbe", "Wecker", "Waffel", "Vase", "Nagel", "Zange", "Figur", "Foto", "Poster", "Nase", "Stempel", "Kissen", "Maske", "Keks", "Abitur", "Liebe", "Autor", "Kunde", "Fest", "Dult", "Prinz", "Kette", "Leise", "Yacht", "Hobby", "Seife", "Teig", "Sonne", "Wolke", "Honig", "Auge", "Ebbe", "Flut", "Mond", "Licht", "Hell", "Neffe", "Nichte", "Onkel", "Tante", "Rose", "Berge", "Hotel", "Kino", "Streit", "Date", "real", "Leben", "Hilfe", "Suche", "Zwang", "Datei", "Nummer", "Zahlen", "Kind", "Jung", "Maus", "Frosch", "Hamster", "Gabel", "Messer", "Teller", "Essen", "Meer", "Berg", "Mund", "Stirn", "Stern", "Mond", "Sonne", "Wasser", "Salz", "Blau", "Gelb", "Erde", "Schnee", "Wolf", "Rose", "Wand", "Tuch", "Buch", "Lava", "Heiss", "Gras", "Wiese", "Rabe", "Honig", "Zahn", "Tuch", "Uran", "Angst", "Oben", "Unten", "Links", "Rechts", "Mode", "Jacke", "Hose", "Tasse", "Strom", "Eisen", "Gold", "Chrom", "Pool", "Kugel", "Affe", "Tiger", "Pfau", "Donner", "Blitz", "Sturm", "Nacht", "Feder", "Brief", "Seife", "Stift", "Wein", "Bier"];
    }

    function initListMedium() {
        listMedium = ["Zwieback", "Maracuja", "Apfelmus", "Kaugummi", "Piercing", "Dompteur", "Zitrone", "Zauberer", "Gelenk", "Rentner", "Roboter", "Marzipan", "Tortilla", "Kontrolle", "Camping", "Schnecke", "Vogelhaus", "Studium", "Weinrot", "Spiegel", "Vampir", "Waggon", "Zahlen", "Telefon", "Mieter", "Seminar", "Ferien", "Verliebt", "Schenken", "Entwurf", "Schmuck", "Kandidat", "Beamter", "Rentner", "Laptop", "Schwarz", "Kajal", "Museum", "Wellness", "Lehrer", "Dozent", "Imker", "Diadem", "Bachelor", "Master", "Diplom", "Doktor", "Metzger", "Kultur", "Programm", "Besuch", "Wagniss", "Peinlich", "Bahnhof", "Sendung", "Zeitung", "Versicherung", "Partner", "Schatz", "Wirklich", "anders", "sparen", "trinken", "Schuhe", "Automat", "Casting", "Schweigen", "Frieden", "Alptraum", "Absage", "Virtuell", "Bildung", "Pfeffer", "Minute", "Sekunde", "Ziege", "Kissen", "Vulkan", "Schlange", "Distel", "Brille", "Horror", "Glocke", "Heizung", "Freund", "Leguan", "Magnesium", "Silber", "Rubin", "Asche", "Jaguar", "Panther", "Leopard", "Specht", "Dirndl"];
    }

    function initListDifficult() {
          listDifficult = ["Tollkirsche", "Pusteblume", "Geldautomat", "Kornblume", "Gartenstuhl", "Marmelade", "Schornstein", "Orangenbaum", "Unternehmen", "Millionen", "Edelstein", "Tamburin", "Nikolaus", "Weihnachten", "Leinwand", "Informatik", "Bilderbuch", "Notizblock", "Moderator", "Mysterium", "Mietauto", "Vorlesung", "Praktikum", "Vintage", "Zuschauer", "Schwimmbad", "Software", "Datenbank", "Hardware", "Freibad", "Instrument", "Almhütte", "Barfusspfad", "Singvogel", "Kurzurlaub", "Annagramm", "Walnuss", "Haselnuss", "Macadamia", "Astronaut", "Silvester", "Professor", "Gymnasium", "Schallplatte", "Augenbraue", "Musikgeschmack", "Karneval", "Woodstock", "Tonspur", "Vitamine", "Staffelei", "Handtuch", "Bademantel", "Schlafsofa", "Tagebuch", "Smartphone", "Zahnspange", "Einkaufen", "Abschluss", "Krankenhaus", "Dinosaurier", "Fensterglas", "Dokumentation", "Reportage", "Nachrichten", "Aufmerksamkeit", "Verstehen", "Waschlappen", "vorstellen", "Konsequenz", "Prinzipien", "Zufrieden", "Darsteller", "Assisitent", "Geheimnis", "Verfolger", "Sicherheit", "Serviette", "Bearbeiten", "Schmetterling", "Marathon", "Eidechse", "Sonnenblume", "Schwimmbad", "Smaragad", "Diamant", "Bernstein", "Fingerhut", "Schneewitchen", "Aschenputtel", "Hochzeit", "Verlobung", "Meteorit", "Weltraum", "Gewitter", "Kuckuck", "Truthahn", "Urkunde", "Pergament", "Oktoberfest", "Vollmond", "Kirchweih", "Nashorn", "Lederhose", "Haftanstalt"];
    }

    function setupMapCities() {
         mapCities = {
             0: "res/drawables/Paris.png",
             1: "res/drawables/nyc.png",
             2: "res/drawables/london.png",
             3: "res/drawables/barcelona.png",
             4: "res/drawables/dubai.png",
             5: "res/drawables/shanghai.png",
             6: "res/drawables/prag.png",
             7: "res/drawables/wien.png",
             8: "res/drawables/moskau.png",
             9: "res/drawables/berlin.png",
             10: "res/drawables/lissabon.png",
             11: "res/drawables/stockholm.png",
             12: "res/drawables/singapur.png",
             13: "res/drawables/melbourne.png",
             14: "res/drawables/sydney.png",
             15: "res/drawables/venedig.png",
             16: "res/drawables/rom.png",
             17: "res/drawables/mailand.png",
             18: "res/drawables/dresden.png",
             19: "res/drawables/pilsen.png",
             20: "res/drawables/münchen.png",
             21: "res/drawables/kopenhagen.png",
             22: "res/drawables/florenz.png",
             23: "res/drawables/tokio.png",
             24: "res/drawables/brüssel.png",
             25: "res/drawables/athen.png",
             26: "res/drawables/budapest.png",
             27: "res/drawables/dublin.png",
             28: "res/drawables/straßburg.png",
             29: "res/drawables/warschau.png",
             30: "res/drawables/bukarest.png",
             31: "res/drawables/zagreb.png",
             32: "res/drawables/bratislava.png",
             33: "res/drawables/sofia.png",
             34: "res/drawables/amsterdam.png",
             35: "res/drawables/regensburg.png",
             36: "res/drawables/compostela.png",
             37: "res/drawables/frankfurt.png",
             38: "res/drawables/mainz.png",
             39: "res/drawables/zürich.png",
             40: "res/drawables/köln.png",
             41: "res/drawables/bangkok.png",
             42: "res/drawables/janeiro.png",
             43: "res/drawables/peru.png",
             44: "res/drawables/helsinki.png",
             45: "res/drawables/agra.png",
             46: "res/drawables/delhi.png",
             47: "res/drawables/kiev.png",
             48: "res/drawables/hanoi.png",
             49: "res/drawables/kualalumpur.png",
             50: "res/drawables/kairo.png",
             51: "res/drawables/mexiko.png"
         };
    }

    function setupMapPromis() {
        mapPromis = {
             0: "res/drawables/becker.png",
             1: "res/drawables/perry.png",
             2: "res/drawables/rihanna.png",
             3: "res/drawables/connor.png",
             4: "res/drawables/catterfeld.png",
             5: "res/drawables/djokovic.png",
             6: "res/drawables/federer.png",
             7: "res/drawables/jauch.png",
             8: "res/drawables/gottschalk.png",
             9: "res/drawables/fischer.png",
             10: "res/drawables/klum.png",
             11: "res/drawables/schweiger.png",
             12: "res/drawables/barak.png",
             13: "res/drawables/meis.png",
             14: "res/drawables/bohlen.png",
             15: "res/drawables/schweinsteiger.png",
             16: "res/drawables/müller.png",
             17: "res/drawables/götze.png",
             18: "res/drawables/hinterseher.png",
             19: "res/drawables/hunziker.png",
             20: "res/drawables/neubauer.png",
             21: "res/drawables/ronaldo.png",
             22: "res/drawables/schweighöfer.png",
             23: "res/drawables/smith.png",
             24: "res/drawables/bieber.png",
             25: "res/drawables/cruise.png",
             26: "res/drawables/graf.png",
             27: "res/drawables/landrut.png",
             28: "res/drawables/nena.png",
             29: "res/drawables/raab.png",
             30: "res/drawables/williams.png",
             31: "res/drawables/kahn.png",
             32: "res/drawables/pocher.png",
             33: "res/drawables/gomez.png",
             34: "res/drawables/neuer.png",
             35: "res/drawables/beckenbauer.png",
             36: "res/drawables/klitschko.png",
             37: "res/drawables/radcliffe.png",
             38: "res/drawables/watson.png",
             39: "res/drawables/grint.png",
             40: "res/drawables/stewart.png",
             41: "res/drawables/pattinson.png",
             42: "res/drawables/merkel.png",
             43: "res/drawables/einstein.png",
             44: "res/drawables/lama.png",
             45: "res/drawables/hawking.png",
             46: "res/drawables/jobs.png",
             47: "res/drawables/jürgens.png",
             48: "res/drawables/monroe.png",
             49: "res/drawables/marzahn.png",
             50: "res/drawables/lagerfeld.png",
             51: "res/drawables/wurst.png"
         };
    }

    function setupCitiesArray() {
        cities = ["paris", "new york", "london", "barcelona", "dubai", "shanghai", "prag", "wien",
                 "moskau", "berlin", "lissabon", "stockholm", "singapur", "melbourne", "sydney", "venedig",
                 "rom", "mailand", "dresden", "pilsen", "münchen", "kopenhagen", "florenz", "tokio",
                 "brüssel", "athen", "budapest", "dublin", "straßburg", "warschau", "bukarest", "zagreb",
                  "bratislava", "sofia", "amsterdam", "regensburg", "santiago de compostela", "frankfurt",
                  "mainz", "zürich", "köln", "bangkok", "rio de janeiro", "peru", "helsinki", "agra",
                  "neu delhi", "kiev", "hanoi", "kuala lumpur", "kairo", "mexiko"];
    }


    function setupPromisArray() {
        promis = ["boris becker", "katy perry", "rihanna", "sarah connor", "yvonne catterfeld", "novak djokovic", "roger federer",
                  "günther jauch", "thomas gottschalk", "helene fischer", "heidi klum", "till schweiger", "elyas m barek",
                  "sylvie meis", "dieter bohlen", "bastian schweinsteiger", "thomas müller", "mario götze", "hansi hinterseher",
                  "michelle hunziker", "christine neubauer", "cristiano ronaldo", "matthias schweighöfer", "will smith",
                  "justin bieber", "tom cruise", "steffi graf", "lena meyer-landrut", "nena", "stefan raab", "serena williams",
                  "oliver kahn", "oliver pocher", "selena gomez", "manuel neuer", "franz beckenbauer", "wladimir klitschko",
                  "daniel radcliffe", "emma watson", "rupert grint", "kristen stewart", "robert pattinson", "angela merkel",
                  "albert einstein", "dalai lama", "stephen hawking", "steve jobs", "udo jürgens", "marilyn monroe",
                  "cindy aus marzahn", "karl lagerfeld", "conchita wurst"];
    }

    function setupMinigamesArray() {
        minigames = ["Allgemeiner Ablauf", "Quiz-wahr-falsch", "Quiz-4-Antworten", "Wortbilder", "Bilderraten",
                     "Hangman", "Snake"];
    }

    function setupMinigamesDescriptionMap() {
        minigamesDescription = {
            "Allgemeiner Ablauf": gameText,
            "Quiz-wahr-falsch": quizWahrFalschText,
            "Quiz-4-Antworten": quiz4AnswersText,
            "Wortbilder": wortbilderText,
            "Bilderraten": bilderratenText,
            "Hangman": hangmanText,
            "Snake": snakeText
         };
    }

    function setupMinigamesScreenMap() {
        minigamesScreen = {
            "Allgemeiner Ablauf": "url('res/drawables/screen-wahr-falsch.png')",
            "Quiz-wahr-falsch": "url('res/drawables/screen-wahr-falsch.png')",
            "Quiz-4-Antworten": "url('res/drawables/screen-4-answers.png')",
            "Wortbilder": "url('res/drawables/screen-wortbilder.png')",
            "Bilderraten": "url('res/drawables/screen-bilderraten.png')",
            "Hangman": "url('res/drawables/screen-hangman.png')",
            "Snake": "url('res/drawables/screen-snake.png')"
         };
    }

    function setupMinigamesHTMLMap() {
        minigamesHTML = {
            "Quiz-wahr-falsch": "games/quiz_wahr_falsch.html",
            "Quiz-4-Antworten": "games/quiz_4_answers.html",
            "Wortbilder": "games/wortbilder.html",
            "Bilderraten": "games/bilderraten.html",
            "Hangman": "games/hangman.html",
            "Snake": "games/snake.html"
        };
    }

    function setupMinigamesTestMap() {
        minigamesTest = {
            "Quiz-wahr-falsch": App.QuizWahrFalsch,
            "Quiz-4-Antworten": App.Quiz4Answers,
            "Wortbilder": App.Wortbilder,
            "Bilderraten": App.Bilderraten,
            "Hangman": App.Hangman,
            "Snake": App.Snake
        };
    }

    function getListEasy() {
        return listEasy;
    }

    function getListMedium() {
        return listMedium;
    }

    function getListDifficult() {
        return listDifficult;
    }

    function getMapCities() {
        return mapCities;
    }

    function getMapPromis() {
        return mapPromis;
    }

    function getArrayCities() {
        return cities;
    }

    function getArrayPromis() {
        return promis;
    }

    function getArrayMinigames() {
        return minigames;
    }

    function getMapMinigamesDescription() {
        return minigamesDescription;
    }

    function getMapMinigamesScreen() {
        return minigamesScreen;
    }

    function getMapMinigamesHTML() {
        return minigamesHTML;
    }

    function getMapMinigamesTest() {
        return minigamesTest;
    }

    function init() {
        setupGameText();
        setupQuizWahrFalschText();
        setupQuiz4AnswersText();
        setupWortbilderText();
        setupBilderratenText();
        setupHangmanText();
        setupSnakeText();
        initListEasy();
        initListMedium();
        initListDifficult();
        setupMapCities();
        setupMapPromis();
        setupCitiesArray();
        setupPromisArray();
        setupMinigamesArray();
        setupMinigamesDescriptionMap();
        setupMinigamesScreenMap();
        setupMinigamesHTMLMap();
        setupMinigamesTestMap();
    }

    return {
        init: init,
        getListEasy: getListEasy,
        getListMedium: getListMedium,
        getListDifficult: getListDifficult,
        getMapCities: getMapCities,
        getMapPromis: getMapPromis,
        getArrayCities: getArrayCities,
        getArrayPromis: getArrayPromis,
        getArrayMinigames: getArrayMinigames,
        getMapMinigamesDescription: getMapMinigamesDescription,
        getMapMinigamesScreen: getMapMinigamesScreen,
        getMapMinigamesHTML: getMapMinigamesHTML,
        getMapMinigamesTest: getMapMinigamesTest
    };
}());
