var App = App || {};
App.Snake = (function () {
    "use strict";

    /* eslint-env browser, jquery, fabric  */

    var canvas,
        canvasBackground,
        snakeHead,
        snakeFood,
        snakeBody,
        barrier,
        lastSnakeBodyPart,
        lastSnakeBodyPartIndex,
        currentSnakeBodyPart,
        snakeArray = [],
        moveSnakeToTop,
        moveSnakeToLeft,
        space = 20,
        speed = 200,
        newSpeed,
        RADIUS_SNAKE_BODY = 20,
        DIAMETER_SNAKE_BODY = 40,
        boundSnakeHead,
        boundSnakeFood,
        boundSnakeBarrier,
        boundSnakeBody,
        foodCollected = 0,
        numFood = 15,
        score,
        message,
        ResultToShow,
        unallowedPosTop,
        unallowedPosLeft,
        unallowedWidth,
        unallowedHeight,
        randomIntNotHeadPosTop,
        randomIntNotHeadPosLeft,
        textResult,
        RESULT_LOST_GAME = "SPIEL VERLOREN",
        RESULT_WON_GAME = "SPIEL GEWONNEN",
        view,
        points,
        direction,
        numBarriers,
        NUM_BARRIERS_START = 3,
        MAX_NUM_BARRIERS = 10,
        snakeBarrierArray = [],
        canvasObjects = [],
        playerName,
        playerString,
        playerJSON,
        numPlaysSnake,
        freePosLeft,
        freePosTop,
        objectWidth,
        objectHeight,
        i;

    //init canvas Background
    function initCanvasBackground() {
        canvasBackground = "res/drawables/snoopy_wortbilder.png";
        canvas.setBackgroundImage(canvasBackground, canvas.renderAll.bind(canvas), {
            width: canvas.width,
            height: canvas.height
        });
    }

    //init Canvas
    function initCanvas() {
        canvas = new fabric.Canvas("snake-canvas");
        canvas.setWidth(600);
        canvas.setHeight(600);
        initCanvasBackground();
    }

    //setup Snake Head
    function setupSnakeHead() {
        var imgElement = document.getElementById("sneak-image");
        snakeHead = new fabric.Image(imgElement, {
            width: 60,
            height: 42
        });
        snakeHead.selectable = false;
        canvas.add(snakeHead);
        snakeHead.set("left", 400);
        snakeHead.set("top", 400);
        snakeHead.setCoords();
        snakeHead.src = "res/drawables/snake_head1.png";
        boundSnakeHead = snakeHead.getBoundingRect();
        canvas.centerObject(snakeHead);
        canvas.renderAll();
        canvasObjects.push(snakeHead);
    }

    //Adds a Body Part to the snake
    //Position depends on the direction, the snake is moving to
    function addSnakeBodyPart() {
        lastSnakeBodyPartIndex = snakeArray.length - 1;
        if (snakeArray.length === 0) {
            lastSnakeBodyPart = snakeHead;
            boundSnakeHead = snakeHead.getBoundingRect();
        } else {
            lastSnakeBodyPart = snakeArray[lastSnakeBodyPartIndex];
        }
        if (direction === "up") {
            snakeBody = new fabric.Circle({
                radius: RADIUS_SNAKE_BODY,
                left: lastSnakeBodyPart.getLeft(),
                top: (lastSnakeBodyPart.getTop() + DIAMETER_SNAKE_BODY),
                fill: "darkgreen"
            });
        } else if (direction === "down") {
            snakeBody = new fabric.Circle({
                radius: RADIUS_SNAKE_BODY,
                left: lastSnakeBodyPart.getLeft(),
                top: (lastSnakeBodyPart.getTop() - DIAMETER_SNAKE_BODY),
                fill: "green"
            });
        } else if (direction === "left") {
            snakeBody = new fabric.Circle({
                radius: RADIUS_SNAKE_BODY,
                left: (lastSnakeBodyPart.getLeft() + DIAMETER_SNAKE_BODY),
                top: lastSnakeBodyPart.getTop(),
                fill: "darkgreen"
            });
        } else if (direction === "right") {
            snakeBody = new fabric.Circle({
                radius: RADIUS_SNAKE_BODY,
                left: (lastSnakeBodyPart.getLeft() - DIAMETER_SNAKE_BODY),
                top: lastSnakeBodyPart.getTop(),
                fill: "green"
            });
        }
        snakeBody.selectable = false;
        canvas.add(snakeBody);
        snakeArray.push(snakeBody);
        canvas.renderAll();
    }

    //returns top and left-position, so that the Object gets a free Position and
    //appears not on another object
    function getFreeTopLeftPos(currentObjectWidth, currentObjectHeight) {
        randomIntNotHeadPosLeft = fabric.util.getRandomInt(50, canvas.height - 50);
        randomIntNotHeadPosTop = fabric.util.getRandomInt(50, canvas.height - 50);
        for (i = 0; i < canvasObjects.length; i++) {
            unallowedPosLeft = canvasObjects[i].left;
            unallowedPosTop = canvasObjects[i].top;
            unallowedHeight = canvasObjects[i].getBoundingRect().height;
            unallowedWidth = canvasObjects[i].getBoundingRect().width;
            if (((randomIntNotHeadPosLeft + currentObjectWidth) > (unallowedPosLeft - space) && randomIntNotHeadPosLeft < (unallowedPosLeft + unallowedWidth + space)) && ((randomIntNotHeadPosTop + currentObjectHeight) > (unallowedPosTop - space) && randomIntNotHeadPosTop < (unallowedPosTop + unallowedHeight + space))) {
                getFreeTopLeftPos(currentObjectWidth, currentObjectHeight);
            }
        }
        freePosLeft = randomIntNotHeadPosLeft;
        freePosTop = randomIntNotHeadPosTop;
    }

    //After it was eaten by snake, food is moved to another position
    function updateFood() {
        objectWidth = snakeFood.getBoundingRect().width;
        objectHeight = snakeFood.getBoundingRect().height;
        getFreeTopLeftPos(objectWidth, objectHeight);
        snakeFood.top = freePosTop;
        snakeFood.left = freePosLeft;
        canvasObjects.shift();
        canvasObjects.unshift(snakeFood);
    }

    function checkTest(result) {
        var test = App.SpielfeldController.getTest();
        if (test === true) {
            var gameContainer = document.querySelector("#game-container");
            gameContainer.style.display = "none";
            document.documentElement.style.overflow = "scroll";
            App.Description.init();
        } else {
            document.documentElement.style.overflow = "scroll";
            App.SpielfeldController.gameFinished(result);
        }
    }

    //show Result
    //depends on lost or won the game
    function showResultGame(result) {
        if(result === true) {
            points += 5;
            message.html("<b>" + points + "</b><br>Punkte").show().fadeOut(4000);
            textResult = RESULT_WON_GAME;
        } else {
            points -= 5;
            message.html("<b>" + points + "</b><br>Punkte").show().fadeOut(4000);
            textResult = RESULT_LOST_GAME;
        }
        ResultToShow = new fabric.Text(textResult, {
            fontSize: 50,
            fontWeight: "bold"
        });
        ResultToShow.selectable = false;
        canvas.add(ResultToShow);
        canvas.centerObject(ResultToShow);
        canvas.renderAll();
    }

    //delete the canvas and prepare the game for the next time
    function deleteCanvas() {
        canvas.clear();
        snakeArray = [];
        snakeBarrierArray = [];
        canvasObjects = [];
        foodCollected = 0;
        speed = 200;
        App.SnakeController.increaseSpeed(speed);
    }

    //check if the game is won
    //When Game is finished it is won
    function checkGameWon() {
        if (numFood === foodCollected) {
            App.SnakeController.moveSnakeStop();
            showResultGame(true);
            setTimeout(checkTest, 2000, true); // !!!!!!!!!!!!!!
            setTimeout(deleteCanvas, 2000);
        }
    }

    //increase Speed of Snake
    function increaseSpeed(actualSpeed) {
        speed = actualSpeed;
    }

    //update the score/Information
    function updateScore() {
        foodCollected++;
        score.innerHTML = foodCollected + "/" + numFood + " Woodstocks gegessen";
        if (foodCollected === 3) {
            newSpeed = 175;
            increaseSpeed(newSpeed);
            App.SnakeController.increaseSpeed(newSpeed);
        }
        if (foodCollected === 5) {
            newSpeed = 150;
            increaseSpeed(newSpeed);
            App.SnakeController.increaseSpeed(newSpeed);
        }
        if (foodCollected === 8) {
            newSpeed = 125;
            increaseSpeed(newSpeed);
            App.SnakeController.increaseSpeed(newSpeed);
        }
        if (foodCollected === 12) {
            newSpeed = 110;
            increaseSpeed(newSpeed);
            App.SnakeController.increaseSpeed(newSpeed);
        }
    }

    function eatFood() {
        updateScore();
        addSnakeBodyPart();
        updateFood();
        checkGameWon();
    }

    //Snake-Head rotate, because it touchs something
    function touchWall() {
        snakeHead.animate("angle", 800, {
            onChange: canvas.renderAll.bind(canvas),
            duration: 2000
        });
    }

    //check collision with Barriers
    function checkBarriers() {
        for (i = 0; i < snakeBarrierArray.length; i++) {
            barrier = snakeBarrierArray[i];
            boundSnakeBarrier = barrier.getBoundingRect();
            if ((barrier.left + boundSnakeBarrier.width) > (snakeHead.left + space) && (barrier.top + boundSnakeBarrier.height) > (snakeHead.top + space) && barrier.left < (snakeHead.left + boundSnakeHead.width - space) && barrier.top < (snakeHead.top + boundSnakeHead.height - space)) {
                return true;
            }
        }
        return false;
    }

    //check collision with Snake-Body
    function checkSnakeBody() {
        for (i = 1; i < snakeArray.length; i++) {
            snakeBody = snakeArray[i];
            boundSnakeBody = snakeBody.getBoundingRect();
            if ((snakeBody.left + boundSnakeBody.width - space) > snakeHead.left && (snakeBody.top + boundSnakeBody.height - space) > snakeHead.top && (snakeBody.left + space) < (snakeHead.left + boundSnakeHead.width) && (snakeBody.top + space) < (snakeHead.top + boundSnakeHead.height)) {
                return true;
            }
        }
        return false;
    }

    //check collision with Food
    function checkCollisionFood() {
        if ((snakeFood.left + boundSnakeFood.width) > snakeHead.left && (snakeFood.top + boundSnakeFood.height) > snakeHead.top && snakeFood.left < (snakeHead.left + boundSnakeHead.width) && snakeFood.top < (snakeHead.top + boundSnakeHead.height)) {
            return true;
        }
        return false;
    }

    //check collision with walls
    function checkWalls() {
        if ((snakeHead.left + boundSnakeHead.width - space) > canvas.width || (snakeHead.top + boundSnakeHead.height - space) > canvas.height || (snakeHead.left + space) < 0 || (snakeHead.top + space) < 0) {
            return true;
        }
        return false;
    }

    //Game finished and is lost
    function stopGameLost() {
        App.SnakeController.moveSnakeStop();
        touchWall();
        showResultGame(false);
        setTimeout(checkTest, 2000, false);
        setTimeout(deleteCanvas, 2000);
    }

    //check all Collsions and check if the game is lost
    function checkCollision() {
        if (checkCollisionFood() === true) {
            eatFood();
        }
        if (checkWalls() === true) {
           stopGameLost();
        }
        if (checkBarriers() === true) {
            stopGameLost();
        }
        if (checkSnakeBody() === true) {
           stopGameLost();
        }
    }

    //move Snake upward
    function moveUp() {
        direction = "up";
        moveSnakeToTop = snakeHead.top - DIAMETER_SNAKE_BODY;
        snakeHead.animate("top", moveSnakeToTop, {
            onChange: canvas.renderAll.bind(canvas),
            duration: speed
        });
        for (i = 0; i < snakeArray.length; i++) {
            currentSnakeBodyPart = snakeArray[i];
            if (i === 0) {
                moveSnakeToTop = snakeHead.top;
                moveSnakeToLeft = snakeHead.left;
            } else {
                moveSnakeToTop = snakeArray[i - 1].top;
                moveSnakeToLeft = snakeArray[i - 1].left;
            }
            currentSnakeBodyPart.animate("top", moveSnakeToTop, {
                onChange: canvas.renderAll.bind(canvas),
                duration: speed
            }).animate("left", moveSnakeToLeft, {
                onChange: canvas.renderAll.bind(canvas),
                duration: speed
            });
        }
        checkCollision();
    }

    //move Snake downward
    function moveDown() {
        direction = "down";
        moveSnakeToTop = snakeHead.top + DIAMETER_SNAKE_BODY;
        snakeHead.animate("top", moveSnakeToTop, {
            onChange: canvas.renderAll.bind(canvas),
            duration: speed
        });
        for (i = 0; i < snakeArray.length; i++) {
            currentSnakeBodyPart = snakeArray[i];
            if (i === 0) {
                moveSnakeToTop = snakeHead.top;
                moveSnakeToLeft = snakeHead.left;
            } else {
                moveSnakeToTop = snakeArray[i - 1].top;
                moveSnakeToLeft = snakeArray[i - 1].left;
            }
            currentSnakeBodyPart.animate("top", moveSnakeToTop, {
                onChange: canvas.renderAll.bind(canvas),
                duration: speed
            }).animate("left", moveSnakeToLeft, {
                onChange: canvas.renderAll.bind(canvas),
                duration: speed
            });
        }
        checkCollision();
    }

    //move Snake to left
    function moveLeft() {
        direction = "left";
        moveSnakeToLeft = snakeHead.left - DIAMETER_SNAKE_BODY;
        snakeHead.animate("left", moveSnakeToLeft, {
            onChange: canvas.renderAll.bind(canvas),
            duration: speed
        });
        for (i = 0; i < snakeArray.length; i++) {
            currentSnakeBodyPart = snakeArray[i];
            if (i === 0) {
                moveSnakeToLeft = snakeHead.left;
                moveSnakeToTop = snakeHead.top;
            } else {
                moveSnakeToLeft = snakeArray[i - 1].left;
                moveSnakeToTop = snakeArray[i - 1].top;
            }
            currentSnakeBodyPart.animate("left", moveSnakeToLeft, {
                onChange: canvas.renderAll.bind(canvas),
                duration: speed
            }).animate("top", moveSnakeToTop, {
                onChange: canvas.renderAll.bind(canvas),
                duration: speed
            });
        }
        checkCollision();
    }

    //move Snake to right
    function moveRight() {
        direction = "right";
        moveSnakeToLeft = snakeHead.left + DIAMETER_SNAKE_BODY;
        snakeHead.animate("left", moveSnakeToLeft, {
            onChange: canvas.renderAll.bind(canvas),
            duration: speed
        });
        for (i = 0; i < snakeArray.length; i++) {
            currentSnakeBodyPart = snakeArray[i];
            if (i === 0) {
                moveSnakeToLeft = snakeHead.left;
                moveSnakeToTop = snakeHead.top;
            } else {
                moveSnakeToLeft = snakeArray[i - 1].left;
                moveSnakeToTop = snakeArray[i - 1].top;
            }
            currentSnakeBodyPart.animate("left", moveSnakeToLeft, {
                onChange: canvas.renderAll.bind(canvas),
                duration: speed
            }).animate("top", moveSnakeToTop, {
                onChange: canvas.renderAll.bind(canvas),
                duration: speed
            });
        }
        checkCollision();
    }

    //setup the Food/Woodstock
    function setupSnakeFood() {
        var imgElement = document.getElementById("food-image");
        snakeFood = new fabric.Image(imgElement, {
            width: 50,
            height: 68
        });
        snakeFood.selectable = false;
        canvas.add(snakeFood);
        snakeFood.set("left", 200);
        snakeFood.set("top", 200);
        snakeFood.setCoords();
        snakeFood.src = "res/drawables/snake_food1.png";
        objectWidth = snakeFood.getBoundingRect().width;
        objectHeight = snakeFood.getBoundingRect().height;
        getFreeTopLeftPos(objectWidth, objectHeight);
        snakeFood.top = freePosTop;
        snakeFood.left = freePosLeft;
        snakeFood.selectable = false;
        boundSnakeFood = snakeFood.getBoundingRect();
        canvas.add(snakeFood);
        canvasObjects.unshift(snakeFood);
        canvas.renderAll();
    }

    //setup the score
    function setupScore() {
        view = document.querySelector("#snake-score");
        score = document.createElement("span");
        score.classList.add("snake-score");
        score.innerHTML = foodCollected + "/" + numFood + " Woodstocks gegessen";
        view.appendChild(score);
    }

    //setup the Barriers
    function setupBarrier() {
        for (var k = 0; k < numBarriers; k++) {
            var imgElement = document.getElementById("barrier-image");
            barrier = new fabric.Image(imgElement, {
                width: 60,
                height: 51
            });
            barrier.selectable = false;
            canvas.add(barrier);
            barrier.set("left", 400);
            barrier.set("top", 400);
            barrier.setCoords();
            barrier.src = "res/drawables/barrier_snake.png";
            objectWidth = barrier.getBoundingRect().width;
            objectHeight = barrier.getBoundingRect().height;
            getFreeTopLeftPos(objectWidth, objectHeight);
            barrier.top = freePosTop;
            barrier.left = freePosLeft;
            barrier.selectable = false;
            canvas.add(barrier);
            canvas.renderAll();
            snakeBarrierArray.push(barrier);
            canvasObjects.push(barrier);
        }
    }

    //numBarriers depends on how often the user played this game
    //numBarriers dictates difficulty
    function chooseDifficult() {
        playerName = App.Start.getPlayerName();
        playerString = localStorage.getItem(playerName);
        playerJSON = JSON.parse(playerString);
        numPlaysSnake = playerJSON.numSnake;
        numBarriers = NUM_BARRIERS_START + numPlaysSnake;
        if (numBarriers > MAX_NUM_BARRIERS) {
            numBarriers = MAX_NUM_BARRIERS;
        }
    }

    function setupMessage() {
        message = $("#game-message");
        message.css({
            top: 100,
            left: 100
        });
    }

    //returns the points the user gained
    function getPoints() {
        return points;
    }

    //init the Background
    function initBackground() {
        document.body.style.backgroundImage = "url('res/drawables/428021.jpg')";
    }

    function init() {
        points = 0;
        initBackground();
        //disable the vertical scrollbar
        document.documentElement.style.overflow = "hidden";
        initCanvas();
        chooseDifficult();
        setupSnakeHead();
        setupBarrier();
        setupSnakeFood();
        setupScore();
        setupMessage();
        App.SnakeController.init();
    }

    return {
        init: init,
        moveUp: moveUp,
        moveDown: moveDown,
        moveLeft: moveLeft,
        moveRight: moveRight,
        getPoints: getPoints
    };
}());
