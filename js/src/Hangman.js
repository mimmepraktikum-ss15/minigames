var App = App || {};
App.Hangman = (function () {
    "use strict";
    /* eslint-env browser, fabric, jquery */
    var canvas,
        WIDTH = 500,
        HEIGHT = 500,
        view,
        points,
        message,
        letter,
        wonTheGame,
        countHeartsDown,
        hangmanGame,
        hangmanView,
        gameFinished,
        TRIES = 8,
        KEY_A = 65,
        KEY_Z = 90,
        KEY_SMALL_A = 97,
        KEY_SMALL_Z = 122,
        hangmanBackground = "res/drawables/hangman_background.png",
        heartImage = "url(res/drawables/heart.png)";

    function initCanvas() {
        canvas = new fabric.Canvas("hangman-canvas");
        canvas.setWidth(WIDTH);
        canvas.setHeight(HEIGHT);
        canvas.setBackgroundImage(hangmanBackground, canvas.renderAll.bind(canvas), {
            width: canvas.width,
            height: canvas.height
        });
    }

    function setupView() {
        view = document.querySelector("#view");
    }

    function setupHearts() {
        for (var i = 0; i < TRIES; i++) {
            var heart = document.createElement("span");
            heart.classList.add("heart");
            heart.style.backgroundImage = heartImage;
            view.appendChild(heart);
        }
    }

    function reduceHearts() {
        view.childNodes[TRIES - countHeartsDown].remove();
        countHeartsDown++;
    }

    function setupMessage() {
        message = $("#game-message");
        message.css({
            top: 100,
            left: 10
        });
    }

    function checkTest(result) {
        var test = App.SpielfeldController.getTest();
        if (test === true) {
            var gameContainer = document.querySelector("#game-container");
            gameContainer.style.display = "none";
            App.Description.init();
        } else {
            App.SpielfeldController.gameFinished(result);
        }
    }

    function onBackClicked() {
        message.finish();
        var button = hangmanView.getBackButton();
        button.removeEventListener("click", onBackClicked, false);
        button.style.display = "none";
        if (wonTheGame === false) {
            checkTest(false);
        } else {
            checkTest(true);
        }
    }

    function showMessage() {
        message.html("<b>" + points + "</b><br>Punkte").show().fadeOut(4000);
    }

    function gameLost() {
        canvas.clear().renderAll();
        document.onkeydown = null;
        hangmanView.drawFigureElements(hangmanGame.getGuessesLeft());
        hangmanView.gameOver();
        gameFinished = true;
        wonTheGame = false;
        points = -5;
        showMessage();
    }

    function gameWon() {
        canvas.clear().renderAll();
        document.onkeydown = null;
        hangmanView.won();
        gameFinished = true;
        wonTheGame = true;
        points = 5;
        showMessage();
    }

    function playHangman() {
        if (gameFinished === false) {
            canvas.clear().renderAll();
            hangmanView.update(letter);
            if (hangmanGame.guess(letter) === true) {
                hangmanView.right();
            } else {
                reduceHearts();
                hangmanView.wrong();
            }
            hangmanGame.getPlaceholders(letter);
            hangmanView.setupTextLabels(letter, hangmanGame.getPlaceholders(letter));
            if (hangmanGame.isGameWon() === true) {
                gameWon();
            } else {
                if (hangmanGame.isGameOver() === true) {
                    gameLost();
                } else {
                    hangmanView.draw(hangmanGame.getGuessesLeft());
                }
            }
        }
    }

    function checkKey(e) {
        letter = String.fromCharCode(e.keyCode);
        var letterCode = e.keyCode;
        if (letterCode >= KEY_A && letterCode <= KEY_Z || letterCode >= KEY_SMALL_A && letterCode <= KEY_SMALL_Z) {
            playHangman();
        }
    }

    function initHangmanGame() {
        hangmanGame = new App.HangmanGame();
        hangmanGame.myWord();
        hangmanGame.placeholderWord();
    }

    function initHangmanView() {
        hangmanView = new App.HangmanView(canvas, WIDTH, HEIGHT, hangmanGame.getHangmanWord(), hangmanGame.getPlaceholders(letter), letter);
        hangmanView.init();
    }

    function getPoints() {
        return points;
    }

    function init() {
        points = 0;
        countHeartsDown = 1;
        gameFinished = false;
        letter = "";
        document.body.style.backgroundImage = "url('res/drawables/401695.jpg')";
        document.onkeydown = checkKey;
        initCanvas();
        setupView();
        setupHearts();
        setupMessage();
        initHangmanGame();
        initHangmanView();
    }

    return {
        init: init,
        getPoints: getPoints,
        onBackClicked: onBackClicked
    };
}());
