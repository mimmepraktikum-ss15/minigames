var App = App || {};
App.HangmanView = function (canvas, width, height, word, placeholders, letter) {
    "use strict";
    /* eslint-env browser,fabric */
    var backButton,
        allGuessedWords = "",
        guessedLetters,
        theWord,
        allGuesses,
        correct,
        wrong,
        won,
        lost,
        figureElements = [],
        snoopyImage = "res/drawables/snoopy.png",
        headImage = "res/drawables/snoopy_head.png",
        BLACK = "#000000",
        BACK = "Zurück",
        ENTER_A_LETTER = "Gib einen Buchstaben ein",
        THE_WORD = "Das Wort: ",
        TIPP = "Deine Tipps: ",
        RIGHT = "Ja, richtig!",
        WRONG = "Nein, falsch!",
        WON = "GEWONNEN!",
        LOST = "VERLOREN!",
        SECRET_WORD = "Das gesuchte Wort: ";

    function setupGuessALetter() {
        var guess = new fabric.Text(ENTER_A_LETTER, {
            top: 100,
            fill: BLACK,
            fontSize: 25,
            selectable: false
        });
        canvas.add(guess);
    }

    function setupGuessedWord() {
        var guessedWord = new fabric.Text(THE_WORD + placeholders, {
            top: 150,
            fill: BLACK,
            fontSize: 25,
            selectable: false
        });
        canvas.add(guessedWord);
    }

    function setupGuessedLetters(letter) {
        guessedLetters = new fabric.Text(TIPP + letter, {
            top: 65,
            fill: BLACK,
            fontSize: 25,
            selectable: false
        });
    }

    function setupWord(placeholders) {
        theWord = new fabric.Text(THE_WORD + placeholders, {
            top: 150,
            fill: BLACK,
            fontSize: 25,
            selectable: false
        });
    }

    function setupAllGuesses() {
        allGuesses = new fabric.Text(allGuessedWords, {
            top: 200,
            fill: BLACK,
            fontSize: 25,
            selectable: false
        });
    }

    function setupCorrect() {
        correct = new fabric.Text(RIGHT, {
            top: 100,
            fill: BLACK,
            fontSize: 25,
            selectable: false
        });
    }

    function setupWrong() {
        wrong = new fabric.Text(WRONG, {
            top: 100,
            fill: BLACK,
            fontSize: 25,
            selectable: false
        });
    }

    function setupTextLabels(letter, placeholders) {
        setupGuessedLetters(letter);
        setupWord(placeholders);
        setupAllGuesses();
        setupCorrect();
        setupWrong();
    }

    function setupBody() {
        var body = new fabric.Line([width / 2 + 165, height / 2 + 3, width / 2 + 163, height / 2 + 80], {
                stroke: BLACK,
                strokeWidth: 7,
                selectable: false
        });
        canvas.add(body);
        figureElements.push(body);
    }

    function setupLeftArm() {
        var leftArm = new fabric.Line([width / 2 + 164, height / 2 + 25, width / 2 + 130, height / 2 + 60], {
                stroke: BLACK,
                strokeWidth: 7,
                selectable: false
        });
        canvas.add(leftArm);
        figureElements.push(leftArm);
    }

    function setupRightArm() {
        var rightArm = new fabric.Line([width / 2 + 165, height / 2 + 25, width / 2 + 200, height / 2 + 60], {
                stroke: BLACK,
                strokeWidth: 7,
                selectable: false
        });
        canvas.add(rightArm);
        figureElements.push(rightArm);
    }

    function setupLeftLeg() {
        var leftLeg = new fabric.Line([width / 2 + 163, height / 2 + 80, width / 2 + 130, height / 2 + 150], {
                stroke: BLACK,
                strokeWidth: 7,
                selectable: false
        });
        canvas.add(leftLeg);
        figureElements.push(leftLeg);
    }

    function setupRightLeg() {
        var rightLeg = new fabric.Line([width / 2 + 163, height / 2 + 80, width / 2 + 196, height / 2 + 150], {
                stroke: BLACK,
                strokeWidth: 7,
                selectable: false
        });
        canvas.add(rightLeg);
        figureElements.push(rightLeg);
    }

    function setupLeftFoot() {
        var leftFoot = new fabric.Line([width / 2 + 130, height / 2 + 150, width / 2 + 113, height / 2 + 160], {
                stroke: BLACK,
                strokeWidth: 7,
                selectable: false
        });
        canvas.add(leftFoot);
        figureElements.push(leftFoot);
    }

    function setupRightFoot() {
        var rightFoot = new fabric.Line([width / 2 + 195, height / 2 + 150, width / 2 + 213, height / 2 + 160], {
                stroke: BLACK,
                strokeWidth: 7,
                selectable: false
        });
        canvas.add(rightFoot);
        figureElements.push(rightFoot);
    }

    function setupStickFigure() {
        setupBody();
        setupLeftArm();
        setupRightArm();
        setupLeftLeg();
        setupRightLeg();
        setupLeftFoot();
        setupRightFoot();
        for (var i = 0; i < figureElements.length; i++) {
            figureElements[i].setVisible(false);
        }
    }

    //Wird in Hangman.js aufgerufen
    function update(letter) {
        allGuessedWords = allGuessedWords + letter + " ";
    }

    function setupSnoopy() {
        fabric.Image.fromURL(snoopyImage, function (snoopy) {
            snoopy.left = 0;
            snoopy.top = 400;
            snoopy.selectable = false;
            canvas.add(snoopy);
        });
    }

    //Wird in Hangman.js aufgerufen
    function showRight() {
        setupSnoopy();
        canvas.add(correct);
        correct.setVisible(true);
    }

    //Wird in Hangman.js aufgerufen
    function showWrong() {
        canvas.add(wrong);
        wrong.setVisible(true);
    }

    function setupWonLabel() {
        won = new fabric.Text(WON, {
            top: 30,
            left: 50,
            fill: BLACK,
            fontSize: 35,
            selectable: false
        });
        canvas.add(won);
    }

    function setupLostLabel() {
        lost = new fabric.Text(LOST, {
            top: 30,
            left: 50,
            fill: BLACK,
            fontSize: 35,
            selectable: false
        });
        canvas.add(lost);
    }

    function secretWord() {
		var secretWordLabel = new fabric.Text(SECRET_WORD + word, {
            top: 140,
            fill: BLACK,
            fontSize: 25,
            selectable: false
        });
        canvas.add(secretWordLabel);
	}

    function goBack() {
        backButton = document.querySelector("#button");
        backButton.style.display = "block";
        backButton.innerHTML = BACK;
        backButton.addEventListener("click", App.Hangman.onBackClicked, false);
    }

    //Wird in Hangman.js aufgerufen
    function setupWon() {
        setupWonLabel();
        secretWord();
        goBack();
    }

    //Wird in Hangman.js aufgerufen
    function setupGameOver() {
        setupLostLabel();
		secretWord();
        goBack();
    }

    function drawTextLabels() {
        canvas.add(guessedLetters);
        canvas.add(theWord);
        canvas.add(allGuesses);
        guessedLetters.setVisible(true);
        theWord.setVisible(true);
        allGuesses.setVisible(true);
    }

    function showHead() {
         fabric.Image.fromURL(headImage, function (head) {
            head.left = width / 2 + 92;
            head.top = height / 2 - 84;
            head.selectable = false;
            canvas.add(head);
        });
    }

    function drawFigureElements(tries) {
        if (tries < 8) {
            showHead();
		}
		if (tries < 7) {
            canvas.add(figureElements[0]);
			figureElements[0].setVisible(true);
		}
		if (tries < 6) {
            canvas.add(figureElements[1]);
			figureElements[1].setVisible(true);
		}
		if (tries < 5) {
            canvas.add(figureElements[2]);
			figureElements[2].setVisible(true);
		}
		if (tries < 4) {
            canvas.add(figureElements[3]);
			figureElements[3].setVisible(true);
		}
		if (tries < 3) {
            canvas.add(figureElements[4]);
			figureElements[4].setVisible(true);
		}
		if (tries < 2) {
            canvas.add(figureElements[5]);
			figureElements[5].setVisible(true);
		}
		if (tries < 1) {
            canvas.add(figureElements[6]);
			figureElements[6].setVisible(true);
		}
    }

    //Wird in Hangman.js aufgerufen
    function draw(tries) {
        drawTextLabels();
        drawFigureElements(tries);
    }

    function getBackButton() {
        return backButton;
    }

    function init() {
        setupGuessALetter();
        setupGuessedWord();
        setupStickFigure();
        setupTextLabels(letter, placeholders);
    }

    return {
        init: init,
        update: update,
        right: showRight,
        wrong: showWrong,
        won: setupWon,
        gameOver: setupGameOver,
        setupTextLabels: setupTextLabels,
        drawFigureElements: drawFigureElements,
        draw: draw,
        getBackButton: getBackButton
    };
};
