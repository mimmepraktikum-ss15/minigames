var App = App || {};
App.Start = (function () {
    "use strict";
    /* eslint-env browser, jquery, fabric  */
    var game,
        view,
        gameTitle,
        title,
        startButton,
        playerField,
        playerNamesString,
        playerNames,
        highscoreButton,
        highscoreView,
        HIGHSCORE = "Highscore",
        KEY = "names",
        NUM = 8,
        KEY_CODE = 13;

    function startSpielfeld() {
        document.onkeydown = null;
        game.style.display = "none";
        App.Spielfeldkarte.init();
    }

    function updateLocalStorage() {
        var name = playerField.val();
        var nameLowerCase = name.toLowerCase();
        if (playerNames.indexOf(nameLowerCase) === -1) {
            playerNames.push(nameLowerCase);
        }
        localStorage[KEY] = JSON.stringify(playerNames);
        localStorage.setItem(KEY, localStorage[KEY]);
    }

    function onLoginButtonClicked() {
        if(playerField.val() !== "") {
            updateLocalStorage();
            startSpielfeld();
        }
    }

    function checkKey(event) {
        var enter = event.keyCode;
        if (enter === KEY_CODE && playerField.val() !== "") {
            updateLocalStorage();
            startSpielfeld();
        }
    }

    function playerFieldAddAutocomplete() {
        playerField.autocomplete({
            source: playerNames
        });
    }

    function checkLocalStorage() {
        if (localStorage.getItem(KEY) === null) {
            playerNames = [];
        } else {
            playerNamesString = localStorage.getItem(KEY);
            playerNames = JSON.parse(playerNamesString);
        }
    }

    function setupStartButton() {
        startButton = document.createElement("input");
        startButton.classList.add("start-button");
        startButton.setAttribute("type", "submit");
        startButton.setAttribute("value", "Login");
        startButton.addEventListener("click", onLoginButtonClicked, false);
        view.append(startButton);
    }

    function setupPlayerField() {
        playerField = $("<input></input>");
        playerField.addClass("player-field");
        playerField.attr("type", "text");
        playerField.attr("required", true);
        playerField.attr("placeholder", "Gib einen Namen ein");
        playerField.attr("autofocus", true);
        playerField.attr("maxlength", NUM);
        checkLocalStorage();
        playerFieldAddAutocomplete();
        view.append(playerField);
    }

    function setupTitle() {
        title = document.createElement("img");
        title.classList.add("title");
        title.setAttribute("src", "res/drawables/minigames.png");
        gameTitle.append(title);
    }

    function setupView() {
        game = document.querySelector("#game-start");
        game.style.backgroundImage = "url('res/drawables/snoopy_background.png')";
        view = $("#start-view");
        gameTitle = $("#game-title");
        setupTitle();
        setupPlayerField();
        setupStartButton();
    }

    function setupBackgroundImage() {
        document.body.style.backgroundImage = "url('res/drawables/377769.png')";
        document.body.style.backgroundRepeat = "no-repeat";
        document.body.style.backgroundSize = "cover";
    }

    function onCloseHighscoreButtonClicked() {
        highscoreView.style.display = "none";
    }

    function onShowHighscoreButtonClicked() {
        highscoreView = document.getElementById("highscore-view-start");
        highscoreView.style.display = "block";
        highscoreView.addEventListener("click", onCloseHighscoreButtonClicked, false);
    }

    function initShowHighscoreButton() {
        highscoreButton = document.getElementsByName("show-highscore-start")[0];
        highscoreButton.style.display = "block";
        if (localStorage.getItem(HIGHSCORE) !== null) {
            App.AddHighscore.initHighscoreTable("highscoreTableStart");
        }
        highscoreButton.addEventListener("click", onShowHighscoreButtonClicked, false);
    }

    function setupShowHighscore() {
        initShowHighscoreButton();
    }

    function getPlayerName() {
        return playerField.val();
    }

    function initMusic() {
        var audio = new Audio("res/music/Peanuts-Theme.mp3");
        audio.addEventListener("ended", function() {
            this.currentTime = 0;
            this.play();
        }, false);
        audio.play();
    }

    function init() {
        initMusic();
        setupBackgroundImage();
        setupView();
        setupShowHighscore();
        document.onkeydown = checkKey;
        App.Arrays.init();
    }

    return {
        init: init,
        getPlayerName: getPlayerName
    };
}());
