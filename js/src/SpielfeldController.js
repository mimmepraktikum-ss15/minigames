var App = App || {};
App.SpielfeldController = (function () {
    "use strict";
    /* eslint-env browser, jquery, fabric  */

    var game,
        playingGame,
        testGame,
        gameContainer,
        gameOver,
        buttonField,
        header,
        buttonGame,
        buttonStart,
        gameLost = false,
        START = "Start",
        SPIELFELD = "Spielfeld";

    function showGameContainer() {
        gameContainer = document.querySelector("#game-container");
        gameContainer.style.display = "block";
    }

    function hideGameContainer() {
        gameContainer = document.querySelector("#game-container");
        gameContainer.style.display = "none";
    }

    function showGame() {
        game = document.querySelector("#game");
        game.style.display = "block";
    }

    function hideGame() {
        game = document.querySelector("#game");
        game.style.display = "none";
    }

    function showGameOver() {
        gameOver = document.querySelector("#game-over");
        gameOver.style.display = "block";
    }

    function updateSnake() {
        if (playingGame === App.Snake) {
            App.Spielfeldkarte.updateJSONSnake();
        }
    }

    function updateWortbilder() {
        if (playingGame === App.Wortbilder) {
            App.Spielfeldkarte.updateJSONWortbilder();
        }
    }

    function test(testResult) {
        testGame = testResult;
    }

    function getTest() {
        return testGame;
    }

    function updateGameMap(result) {
        updateSnake();
        updateWortbilder();
        var points = playingGame.getPoints();
        var pointsMessage = $("#points-message");
        gameLost = false;
        if (result === false) {
            App.Spielfeldkarte.initKeyboard();
            App.Spielfeldkarte.updateJSONPoints(points, gameLost);
            App.Spielfeldkarte.updateJSONPlayerScore(points);
            App.Spielfeldkarte.updatePoints(gameLost);
            App.Spielfeldkarte.updateJSONHearts(gameLost);
            App.Spielfeldkarte.removeHearts();
            pointsMessage.html("<b>" + points + "</b> Punkte verloren").show().fadeOut(4000);
        } else {
            App.Spielfeldkarte.initKeyboard();
            App.Spielfeldkarte.updateJSONPoints(points, gameLost);
            App.Spielfeldkarte.updateJSONPlayerScore(points);
            App.Spielfeldkarte.updatePoints();
            App.Spielfeldkarte.updateJSONLevels(gameLost);
            App.Spielfeldkarte.moveSnoopyForward(gameLost);
            pointsMessage.html("<b>" + points + "</b> Punkte gewonnen").show().fadeOut(4000);
        }
    }

    function onStartButtonClicked() {
        gameLost = true;
        gameOver.style.display = "none";
        App.Spielfeldkarte.updateJSONPoints(0, gameLost);
        App.Spielfeldkarte.updateJSONHearts(gameLost);
        App.Spielfeldkarte.updateJSONLevels(gameLost);
        location.reload();
    }

    function restartSpielfeldkarte() {
        App.Spielfeldkarte.initKeyboard();
        App.Spielfeldkarte.updateJSONPoints(0, gameLost);
        App.Spielfeldkarte.updateJSONHearts(gameLost);
        App.Spielfeldkarte.updatePoints(gameLost);
        App.Spielfeldkarte.initHearts(5);
        var numLevels = App.Spielfeldkarte.getJSONLevels();
        if (numLevels > 0) {
            App.Spielfeldkarte.updateJSONLevels(gameLost);
            App.Spielfeldkarte.moveSnoopyForward(gameLost);
        }
    }

    function onGameButtonClicked() {
        gameLost = true;
        gameOver.style.display = "none";
        buttonGame.remove();
        buttonStart.remove();
        showGame();
        header.style.display = "block";
        restartSpielfeldkarte();
    }

    function setupGameOverSnoopy() {
        var gameOverSnoopy = document.querySelector("#game-over-snoopy");
        gameOverSnoopy.style.backgroundImage = "url('res/drawables/snoopy-lost.png')";
    }

    function setupGameOverImage() {
        var gameOverImage = document.querySelector("#game-over-image");
        gameOverImage.style.backgroundImage = "url('res/drawables/gameover.png')";
    }
    function setupHeader() {
        header = document.querySelector("header");
        header.style.display = "none";
    }

    function setupButtonField() {
        buttonField = document.querySelector("#buttons");
    }

    function setupButtonStart() {
        buttonStart = document.createElement("span");
        buttonStart.classList.add("button-start");
        buttonStart.innerHTML = START;
        buttonStart.addEventListener("click", onStartButtonClicked, false);
        buttonField.appendChild(buttonStart);
    }

    function setupButtonGame() {
        buttonGame = document.createElement("span");
        buttonGame.classList.add("button-game");
        buttonGame.innerHTML = SPIELFELD;
        buttonGame.addEventListener("click", onGameButtonClicked, false);
        buttonField.appendChild(buttonGame);
    }

    function initGameOver() {
        setupHeader();
        setupGameOverImage();
        setupGameOverSnoopy();
        setupButtonField();
        setupButtonStart();
        setupButtonGame();
    }

    function gameFinished(result) {
        hideGameContainer();
        var hearts = App.Spielfeldkarte.getJSONHearts();

        if (hearts === 0 && result === false) {
            showGameOver();
            App.Spielfeldkarte.removeHearts();
            initGameOver();
        } else {
            showGame();
            updateGameMap(result);
        }
        document.body.style.backgroundImage = "url('res/drawables/377769.png')";
    }

    function onGameQuizWahrFalschLoad() {
        showGameContainer();
        App.QuizWahrFalsch.init();
        playingGame = App.QuizWahrFalsch;
    }

    function onGameQuiz4AnswersLoad() {
        showGameContainer();
        App.Quiz4Answers.init();
        playingGame = App.Quiz4Answers;
    }

    function onGameWortbilderLoad() {
        showGameContainer();
        App.Wortbilder.init();
        playingGame = App.Wortbilder;
    }

    function onGameHangmanLoad() {
        showGameContainer();
        App.Hangman.init();
        playingGame = App.Hangman;
    }

    function onGameSnakeLoad() {
        showGameContainer();
        App.Snake.init();
        playingGame = App.Snake;
    }

    function onGameBilderratenLoad() {
        showGameContainer();
        App.Bilderraten.init();
        playingGame = App.Bilderraten;
    }

    function chooseRandomGame() {
        var gameNumber = fabric.util.getRandomInt(0, 5);
        switch (gameNumber) {
        case 0:
            gameContainer = $("#game-container").load("games/quiz_wahr_falsch.html", onGameQuizWahrFalschLoad);
            break;
        case 1:
            gameContainer = $("#game-container").load("games/quiz_4_answers.html", onGameQuiz4AnswersLoad);
            break;
        case 2:
            gameContainer = $("#game-container").load("games/wortbilder.html", onGameWortbilderLoad);
            break;
        case 3:
            gameContainer = $("#game-container").load("games/hangman.html", onGameHangmanLoad);
            break;
        case 4:
            gameContainer = $("#game-container").load("games/snake.html", onGameSnakeLoad);
            break;
        case 5:
            gameContainer = $("#game-container").load("games/bilderraten.html", onGameBilderratenLoad);
            break;
        default:
            gameContainer = "";
        }
    }

    function loadGameWon() {
        showGameContainer();
        App.GameWon.init();
    }

    function startGameWon() {
         document.onkeydown = null;
         hideGame();
         gameContainer = $("#game-container").load("games/game_won.html", loadGameWon);
    }

    function onStartGameButtonClicked() {
        document.onkeydown = null;
        hideGame();
        test(false);
        chooseRandomGame();
    }

    return {
        onStartGameButtonClicked: onStartGameButtonClicked,
        gameFinished: gameFinished,
        startGameWon: startGameWon,
        test: test,
        getTest: getTest
    };
}());
