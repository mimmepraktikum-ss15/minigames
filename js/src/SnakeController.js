var App = App || {};
App.SnakeController = (function () {
    "use strict";
/* eslint-env browser, jquery, fabric  */
    var KEY_UP = 38,
        KEY_DOWN = 40,
        KEY_LEFT = 37,
        KEY_RIGHT = 39,
        KEY_W = 87,
        KEY_S = 83,
        KEY_A = 65,
        KEY_D = 68,
        moveInterval,
        speed = 200,
        currentKeyCode;

    //init Keydown Listener
    //the user can control the snake either with WASD or with arrow keys
    function initMoveKeyPress() {
        $(document).keydown(function (e) {
            e = e || window.event;
            if (currentKeyCode !== e.keyCode && e.keyCode !== undefined) {
                clearInterval(moveInterval);
                moveInterval = undefined;
            }
            if (moveInterval === undefined) {
                currentKeyCode = e.keyCode;
                e = e || window.event;
                if (e.keyCode === KEY_UP || e.keyCode === KEY_W) {
                    moveInterval = setInterval(App.Snake.moveUp, speed);
                } else if (e.keyCode === KEY_DOWN || e.keyCode === KEY_S) {
                    moveInterval = setInterval(App.Snake.moveDown, speed);
                } else if (e.keyCode === KEY_LEFT || e.keyCode === KEY_A) {
                    moveInterval = setInterval(App.Snake.moveLeft, speed);
                } else if (e.keyCode === KEY_RIGHT || e.keyCode === KEY_D) {
                    moveInterval = setInterval(App.Snake.moveRight, speed);
                }
            }
        });
    }

    //removes keydown-Listener
    //the snake doesn't move anymore
    function moveSnakeStop() {
        $(document).unbind("keydown");
        clearInterval(moveInterval);
        moveInterval = undefined;
    }

    function increaseSpeed(actualSpeed) {
        speed = actualSpeed;
    }

    function init() {
        initMoveKeyPress();
    }

    return {
        init: init,
        moveSnakeStop: moveSnakeStop,
        increaseSpeed: increaseSpeed
    };
}());
