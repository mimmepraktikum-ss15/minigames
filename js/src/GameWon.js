var App = App || {};
App.GameWon = (function () {
    "use strict";
    /* eslint-env browser, jquery, fabric  */

    var deletePlayerButton,
        view,
        playerScore,
        playerName,
        playerString,
        playerJSON,
        highscoreJSON,
        highscoreString,
        WON_TEXT = ",<br/> DU HAST <br/> GEWONNEN !!!",
        HIGHSCORE = "Highscore";

    function initBackground() {
        document.body.style.backgroundImage = "url('res/drawables/614159.jpg')";
    }

    //removes the current player from localStorage
    //Start.js is executed
    function onDeletePlayerButtonClicked() {
        localStorage.removeItem(App.Start.getPlayerName());
        location.reload();
    }

    //init the DeletePlayerButton and add a click-Listener to it
    function initDeletePlayerButton() {
        deletePlayerButton = document.getElementsByName("won-delete-player")[0];
        deletePlayerButton.addEventListener("click", onDeletePlayerButtonClicked, false);
    }

    //init the text view and adds the current player name to the text
    function initTextView() {
        view = document.querySelector("#game-won-text");
        view.innerHTML = playerName.toUpperCase() + WON_TEXT;
    }

    //gets the actual player score
    function getPlayerScore() {
        playerName = App.Start.getPlayerName();
        playerString = localStorage.getItem(playerName);
        playerJSON = JSON.parse(playerString);
        playerScore = playerJSON.playerScore;
        return playerScore;
    }

    //save score and name from the winner in LocalStorage-Highscore
    function addPlayerToHighscoreJSON() {
        highscoreString = localStorage.getItem(HIGHSCORE);
        highscoreJSON = JSON.parse(highscoreString);
        highscoreJSON.DATA.push({"playerName": playerName, "playerScore": playerScore});
        highscoreString = JSON.stringify(highscoreJSON);
        localStorage.setItem(HIGHSCORE, highscoreString);
    }

    //gets the winner's name and total score
    function getHighscoreData() {
        playerName = App.Start.getPlayerName();
        playerScore = getPlayerScore();
    }

    //if there isn't a Highscore yet, the Highscore JSON will be generated
    //and be added to the localStorage with "Highscore" as key
    function initHighscore() {
        highscoreJSON = {
            "DATA": [
                {"playerName": playerName,
                 "playerScore": playerScore}
            ]
        };
        highscoreString = JSON.stringify(highscoreJSON);
        localStorage.setItem(HIGHSCORE, highscoreString);
    }

    //setup the Highscore/-Data
    //if there is a Highscore already, only the current player will be add to it.
    //otherwise the Highscore will be generated and add to localStorage
    function initView() {
        getHighscoreData();
        if (localStorage.getItem(HIGHSCORE) === null) {
            initHighscore();
        } else {
            addPlayerToHighscoreJSON();
        }
        App.AddHighscore.initHighscoreTable("highscoreTable");
        initTextView();
        App.AddHighscore.initCloseHighscoreButton();
    }

    //called, when game is won
    //init Background, Buttons and Highscore
    function init() {
        initBackground();
        initView();
        initDeletePlayerButton();
        App.AddHighscore.initShowHighscoreButton("show-highscore", "highscore-view");
    }

    return {
        init: init
    };

}());
